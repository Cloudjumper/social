package org.drekorik.social.dbinter;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by cloudjumper on 3/4/17.
 */
public class MySqlDBConnectionPoolTest {
    @Test
    public void getConnection() throws Exception {
        try(
                Connection connection = MySqlDBConnectionPool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM User ");
                ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            while (resultSet.next()){
                System.out.println(resultSet.getString("login"));
            }
        }
    }

}