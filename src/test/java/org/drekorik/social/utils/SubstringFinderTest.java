package org.drekorik.social.utils;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by cloudjumper on 3/13/17.
 */
public class SubstringFinderTest {
    @Test
    public void find() throws Exception {
        String s = "/user/ngolubev";
        String p = "(?<=/user/)[a-zA-Z]+$";
        System.out.println(SubstringFinder.find(s, p));
    }

}