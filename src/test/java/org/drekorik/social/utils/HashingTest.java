package org.drekorik.social.utils;

import org.hamcrest.core.Is;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.core.Is.*;
import static org.junit.Assert.*;

/**
 * Created by cloudjumper on 3/7/17.
 */
public class HashingTest {
    /**
     * This test can not be past anyway. It generate new salt every time.
     * I need it to get salt for using in hashing passwords
     * @throws Exception
     */
    @Test
    @Ignore
    public void genSalt() throws Exception {
        System.out.println(Hashing.genSalt(2));
    }

    @Test
    public void hash() throws Exception {
        assertThat(Hashing.hash("password"), is("jacHYrricaHKmQkDQUf2AlTK10xBIGi"));
    }

    @Test
    public void fastHash() throws Exception {
        System.out.println(Hashing.fastHash("test"));
    }

    @Test
    public void hashWithCustomSalt() throws Exception {
        String salt1 = "$2a$05$fUs64gI0WkF3eiyXuQzaY.";
        String salt2 = "$2a$05$0fduo8Bp1Fp9pcm9Vp9ifu";
        assertThat(Hashing.hash("password", salt1), is("l.K.qyF5LkEmIISp3.xcB8tiWx98s.G"));
        assertThat(Hashing.hash("password", salt2), is("9ovt55LHe.sp/k5kwa9v090cnmYCW02"));
    }

    @Test
    public void check() throws Exception {
        assertTrue(Hashing.check("password", "jacHYrricaHKmQkDQUf2AlTK10xBIGi"));
    }

    @Test
    public void checkWithCustomSalt() throws Exception {
        String salt1 = "$2a$05$fUs64gI0WkF3eiyXuQzaY.";
        String salt2 = "$2a$05$0fduo8Bp1Fp9pcm9Vp9ifu";
        assertTrue(Hashing.check("password", "l.K.qyF5LkEmIISp3.xcB8tiWx98s.G", salt1));
        assertTrue(Hashing.check("password", "9ovt55LHe.sp/k5kwa9v090cnmYCW02", salt2));
    }
}