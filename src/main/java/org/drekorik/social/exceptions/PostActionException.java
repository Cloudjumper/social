package org.drekorik.social.exceptions;

/**
 * Created by cloudjumper on 3/14/17.
 */
public class PostActionException extends Exception {
    public PostActionException() {
        super();
    }

    public PostActionException(String message) {
        super(message);
    }

    public PostActionException(String message, Throwable cause) {
        super(message, cause);
    }

    public PostActionException(Throwable cause) {
        super(cause);
    }

    protected PostActionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
