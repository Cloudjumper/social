package org.drekorik.social.exceptions;

/**
 * Created by cloudjumper on 3/9/17.
 */
public class UserActionsException extends Exception {
    public UserActionsException() {
        super();
    }

    public UserActionsException(String message) {
        super(message);
    }

    public UserActionsException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserActionsException(Throwable cause) {
        super(cause);
    }

    protected UserActionsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
