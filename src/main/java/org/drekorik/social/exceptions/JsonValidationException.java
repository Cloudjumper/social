package org.drekorik.social.exceptions;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by cloudjumper on 3/9/17.
 */
public class JsonValidationException extends RuntimeException {
    private ValidationException e;

    public JsonValidationException() {
        super();
    }

    public JsonValidationException(String message) {
        super(message);
    }

    public JsonValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsonValidationException(Throwable cause) {
        super(cause);
    }

    public JsonValidationException(ValidationException e) {
        this.e = e;
    }

    protected JsonValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public String getErrors(){
        if (this.e == null)
            return "";
        return this.e.getCausingExceptions().stream()
                .map(ValidationException::getPointerToViolation)
                .map(s -> s.replace("#/",""))
                .collect(Collectors.joining(":"));
    }
}
