package org.drekorik.social.exceptions;

/**
 * Created by cloudjumper on 3/13/17.
 */
public class PictureActionsException extends Exception {
    public PictureActionsException() {
        super();
    }

    public PictureActionsException(String message) {
        super(message);
    }

    public PictureActionsException(String message, Throwable cause) {
        super(message, cause);
    }

    public PictureActionsException(Throwable cause) {
        super(cause);
    }

    protected PictureActionsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
