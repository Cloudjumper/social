package org.drekorik.social.models;

import lombok.*;

/**
 * Created by cloudjumper on 3/9/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Picture {
    private String id;
    private String webPath;
    private String filePath;
}
