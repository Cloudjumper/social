package org.drekorik.social.models;

import lombok.*;

import java.time.LocalDate;

/**
 * Created by cloudjumper on 3/4/17.
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class User {
    private long id;
    private String login;
    private String pass;
    private String email;
    private String firstName;
    private String secondName;
    private LocalDate birthDate;
    private String zone;
    private Picture avatar;
}
