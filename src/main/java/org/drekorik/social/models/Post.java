package org.drekorik.social.models;

import lombok.*;

import java.time.LocalDateTime;

/**
 * Created by cloudjumper on 3/14/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Post {
    private long id;
    private User user;
    private String text;
    private LocalDateTime date;
    private double lat;
    private double lon;
    private Picture picture;
}
