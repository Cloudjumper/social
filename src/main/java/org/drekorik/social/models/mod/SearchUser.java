package org.drekorik.social.models.mod;

import lombok.*;
import org.drekorik.social.models.Picture;
import org.drekorik.social.models.User;

import java.time.LocalDate;

/**
 * Created by cloudjumper on 3/15/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SearchUser extends User {
    private long id;
    private String login;
    private String pass;
    private String email;
    private String firstName;
    private String secondName;
    private LocalDate birthDate;
    private String zone;
    private Picture avatar;
    private boolean isFollow;
}
