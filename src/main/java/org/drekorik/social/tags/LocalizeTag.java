package org.drekorik.social.tags;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.drekorik.social.utils.UTF8ResourceBundleControl;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by cloudjumper on 3/5/17.
 */

/**
 * @version 0.1 First try to create analogue of {@link javax.servlet.jsp.jstl.fmt},
 * because it isn't work with cyrillic characters
 * @deprecated because now idea automatically use (or did something same) native2ascii
 * and JSTL tag {@link javax.servlet.jsp.jstl.fmt} now correctly read russian property file
 * I turn it on via File -> Settings -> Editor -> File Encodings ->
 * enable Transparent native-to-ascii conversion
 */
@Deprecated
public class LocalizeTag extends TagSupport {
    private static final Logger LOGGER = LogManager.getLogger("FASTD");
    private String lang = "en";
    private String key = "en";

    private final static ResourceBundle RU_BUNDLE = ResourceBundle.getBundle("localization",
            new Locale("ru"),
            new UTF8ResourceBundleControl());
    private final static ResourceBundle EN_BUNDLE = ResourceBundle.getBundle("localization",
            new Locale("en"),
            new UTF8ResourceBundleControl());

    public void setLang(String lang){
        this.lang = lang.toLowerCase();
    }

    public void setKey(String key){
        this.key = key;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            JspWriter writer = pageContext.getOut();
            switch (this.lang){
                case "ru":
                case "ru_ru":
                case "ru-ru":{
                    writer.println(RU_BUNDLE.getString(this.key));
                    break;
                }
                case "en":
                case "en_en":
                case "en_us":
                case "en-en":
                case "en-us":{
                    writer.println(EN_BUNDLE.getString(this.key));
                    break;
                }
                default:{
                    LOGGER.warn(String.format("Localization from client was: %1$s", this.lang));
                    writer.println(EN_BUNDLE.getString(this.key));
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}
