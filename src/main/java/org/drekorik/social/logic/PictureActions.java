package org.drekorik.social.logic;

import lombok.SneakyThrows;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.drekorik.social.dao.PictureDao;
import org.drekorik.social.dao.PictureDaoMySql;
import org.drekorik.social.dao.PictureDaoProxy;
import org.drekorik.social.exceptions.PictureActionsException;
import org.drekorik.social.models.Picture;
import org.drekorik.social.utils.Hashing;

import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.*;

public class PictureActions {
    private static final Logger LOGGER = LogManager.getLogger("FASTD");
    private static final Map<String, String> EXTENSIONS = new HashMap<>();
    static {
        EXTENSIONS.put("image/jpeg", "jpeg");
        EXTENSIONS.put("image/png", "png");
    }
    private static String picStorage;
    private static String picWebLink;
    private final ResourceBundle bundle;
    private PictureDao pictureDao;

    // This is hardcode path, there can not be exception
    @SneakyThrows
    public PictureActions(Locale locale) {
        this.bundle = ResourceBundle.getBundle("localization", locale);
        pictureDao = PictureDaoProxy.getInstance();
        try(InputStream stream = PictureActions.class.getResourceAsStream("/pic.properties")){
            Properties properties = new Properties();
            properties.load(stream);
            picStorage = new File(properties.getProperty("storage")).getCanonicalPath().intern();
            picWebLink = new File(properties.getProperty("webLink")).getCanonicalPath().intern();
        }
    }

    public PictureActions(String locale) {
        this(new Locale(locale));
    }

    public Picture save(Part file) throws PictureActionsException {
        String extension = EXTENSIONS.get(file.getContentType());
        if (extension == null){
            throw new PictureActionsException(String.format("{\"error\": \"%1$s\"}",
                    bundle.getString("back.pic.save.err.badFormat")));
        }
        String hash = Hashing.fastRandomHash(String.format("%1$s%2$d", file.getContentType(), file.getSize()));
        String base64 = Hashing.toBase64(hash);
        String fileName = String.format("%1$s.%2$s", base64, extension);
        Picture picture = new Picture(base64, picWebLink + "/" + fileName, picStorage + "/" + fileName);
//        if (new File(picStorage + fileName).exists())
//            return null;
        try {
            picture = pictureDao.insert(picture);
            file.write(picture.getFilePath());
        } catch (IOException e) {
            try {
                pictureDao.delete(picture);
            } catch (SQLException e1) {
                LOGGER.error("Exception on delete from DB, when was exception on write, fail");
                LOGGER.error(e1.getMessage());
                throw new PictureActionsException(String.format("{\"error\": \"%1$s\"",
                        bundle.getString("back.unknownException")));
            }
            LOGGER.error(e.getMessage());
            throw new PictureActionsException(String.format("{\"error\": \"%1$s\"",
                    bundle.getString("back.unknownException")));
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new PictureActionsException(String.format("{\"error\": \"%1$s\"",
                    bundle.getString("back.unknownException")));
        }
        return picture;
    }
}
