package org.drekorik.social.logic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.drekorik.social.controllers.page.RegistrationPage;
import org.drekorik.social.dao.UserDaoProxy;
import org.drekorik.social.dao.UserDao;
import org.drekorik.social.exceptions.UserActionsException;
import org.drekorik.social.models.User;
import org.drekorik.social.models.mod.SearchUser;
import org.drekorik.social.utils.Hashing;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;


public class UserActions {
    private static final Logger LOGGER = LogManager.getLogger("FASTD");
    private final ResourceBundle bundle;
    private UserDao userDao;

    public UserActions(String locale) {
        this(new Locale(locale));
    }

    public UserActions(Locale locale) {
        this.bundle = ResourceBundle.getBundle("localization", locale);
        userDao = UserDaoProxy.getInstance();
    }

    @SuppressWarnings("Duplicates")
    public User register(JSONObject userJson) throws UserActionsException {
        try(InputStream resource = RegistrationPage.class.getResourceAsStream("/json/registrationSchema.json")) {
            Schema schema = SchemaLoader.load(new JSONObject(new JSONTokener(resource)));
            schema.validate(userJson);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ValidationException e){
            JSONObject response = new JSONObject();
            if (e.getCausingExceptions().size() > 0){
                e.getCausingExceptions().forEach(e1 -> {
                    LOGGER.error(e1.getMessage());
                    String elem = e1.getPointerToViolation().replace("#/", "");
                    response.put(elem, bundle.getString("back.user.reg.err." + elem));
                });
            }
            else {
                LOGGER.error(e.getMessage());
                String elem = e.getPointerToViolation().replace("#/", "");
                response.put(elem, bundle.getString("back.user.reg.err." + elem));
            }
            LOGGER.error(String.format("Incoming json is: %1$s", userJson.toString()));
            throw new UserActionsException(response.toString());
        }

        String login = userJson.getString("login").intern();
        String pass = Hashing.hash(userJson.getString("pass")).intern();
        String email = userJson.getString("email").intern();
        String firstName =  userJson.getString("firstName").intern();
        String secondName = userJson.getString("secondName").intern();
        LocalDate birthDate = LocalDate.parse(userJson.getString("birthDate"), DateTimeFormatter.ofPattern("d-M-y"));
        String zone = userJson.getString("zone").intern();

        try {
            int duplicates = userDao.duplicateCheck(login, email);
            if (duplicates == 0){
                User user = new User(-1, login, pass, email, firstName, secondName, birthDate, zone, null);
                userDao.insert(user);
                return user;
            }
            else{
                switch (duplicates){
                    case 1:{
                        throw new UserActionsException(new JSONObject()
                                .put("login", bundle.getString("back.user.reg.err.duplicate.login")).toString());
                    }
                    case 2:{
                        throw new UserActionsException(new JSONObject()
                                .put("email", bundle.getString("back.user.reg.err.duplicate.email")).toString());
                    }
                    case 3:{
                        throw new UserActionsException(new JSONObject()
                                .put("login", bundle.getString("back.user.reg.err.duplicate.login"))
                                .put("email", bundle.getString("back.user.reg.err.duplicate.email"))
                                .toString());
                    } default:{
                        LOGGER.error("Duplicates check return wrong result");
                        throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                                bundle.getString("back.unknownException")));
                    }
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                    bundle.getString("back.unknownException")));
        }
    }

    @SuppressWarnings("Duplicates")
    public User update(JSONObject userJson, User oldUser) throws UserActionsException {
        try(InputStream resource = RegistrationPage.class.getResourceAsStream("/json/updateSchema.json")) {
            Schema schema = SchemaLoader.load(new JSONObject(new JSONTokener(resource)));
            schema.validate(userJson);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ValidationException e){
            JSONObject response = new JSONObject();
            if (e.getCausingExceptions().size() > 0){
                e.getCausingExceptions().forEach(e1 -> {
                    LOGGER.error(e1.getMessage());
                    String elem = e1.getPointerToViolation().replace("#/", "");
                    response.put(elem, bundle.getString("back.user.reg.err." + elem));
                });
            }
            else {
                LOGGER.error(e.getMessage());
                String elem = e.getPointerToViolation().replace("#/", "");
                response.put(elem, bundle.getString("back.user.reg.err." + elem));
            }
            LOGGER.error(String.format("Incoming json is: %1$s", userJson.toString()));
            throw new UserActionsException(response.toString());
        }

        String email = userJson.getString("email").intern();
        String firstName =  userJson.getString("firstName").intern();
        String secondName = userJson.getString("secondName").intern();
        LocalDate birthDate = LocalDate.parse(userJson.getString("birthDate"), DateTimeFormatter.ofPattern("d-M-y"));
        String zone = userJson.getString("zone").intern();

        try {
            User user = new User(oldUser.getId(), oldUser.getLogin(), "", email, firstName, secondName, birthDate, zone, null);
            userDao.update(user);
            return user;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                    bundle.getString("back.unknownException")));
        }
    }

    public User get(String login) throws UserActionsException {
        try {
            return userDao.select(login);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                    bundle.getString("back.unknownException")));
        }
    }

    public User logIn(JSONObject data) throws UserActionsException {
        try(InputStream resource = RegistrationPage.class.getResourceAsStream("/json/logInSchema.json")) {
            Schema schema = SchemaLoader.load(new JSONObject(new JSONTokener(resource)));
            schema.validate(data);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ValidationException e){
            LOGGER.error(e.getMessage());
            LOGGER.error(String.format("Incoming json is: %1$s", data.toString()));
            throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                    bundle.getString("back.user.logIn.err")));
        }

        try {
            if (!userDao.logInCheck(data.getString("login"), Hashing.hash(data.getString("pass"))))
                throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                        bundle.getString("back.user.logIn.err")));
            return get(data.getString("login"));
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                    bundle.getString("back.user.logIn.err")));
        }
    }

    public void avatarChange(User user) throws UserActionsException {
        try {
            userDao.avatarChange(user);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                    bundle.getString("back.unknownException")));
        }
    }

    public List<SearchUser> findUsers(String login) throws UserActionsException {
        try {
            return userDao.findUser(login);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                    bundle.getString("back.unknownException")));
        }
    }

    public void follow(User user1, User user2) throws UserActionsException {
        try {
            if (user1.getLogin().equals(user2.getLogin()))
                throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                        bundle.getString("back.user.follow.err.self")));
            userDao.follow(user1, user2);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                    bundle.getString("back.unknownException")));
        }
    }

    public void stopFollow(User user1, User user2) throws UserActionsException {
        try {
            if (user1.getLogin().equals(user2.getLogin()))
                throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                        bundle.getString("back.user.follow.err.self")));
            userDao.stopFollow(user1, user2);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                    bundle.getString("back.unknownException")));
        }
    }

    public List<User> subscriptions(User user) throws UserActionsException {
        try {
            return userDao.subscription(user);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new UserActionsException(String.format("{\"error\": \"%1$s\"}",
                    bundle.getString("back.unknownException")));
        }
    }
}
