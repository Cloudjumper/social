package org.drekorik.social.logic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.drekorik.social.dao.PostDao;
import org.drekorik.social.dao.PostDaoProxy;
import org.drekorik.social.exceptions.PostActionException;
import org.drekorik.social.models.Post;
import org.drekorik.social.models.User;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.*;

public class PostActions {
    private static final Logger LOGGER = LogManager.getLogger("FASTD");
    private final ResourceBundle bundle;
    private PostDao postDao;

    public PostActions(String locale) {
        this(new Locale(locale));
    }

    public PostActions(Locale locale) {
        this.bundle = ResourceBundle.getBundle("localization", locale);
        postDao = PostDaoProxy.getInstance();
    }

    public List<Post> getUsersPosts(String login, int offset, int count) throws PostActionException {
        try {
            return postDao.getPosts(login, offset, count + 1);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new PostActionException(String.format("{\"error\": \"%1$s\"",
                    bundle.getString("back.unknownException")));
        }
    }

    @SuppressWarnings("Duplicates")
    public JSONObject getJsonUsersPosts(String login, int offset, int count) throws PostActionException {

        List<Post> posts = getUsersPosts(login, offset, count);
        boolean nextPage = posts.size() > count;
        boolean prevPage = offset != 0;

        JSONObject jPosts = new JSONObject();
        JSONObject jPost;
        for(Post post : posts){
            jPost = new JSONObject();
            jPost.put("postId", post.getId());
            jPost.put("postText", post.getText());
            jPost.put("postDate", post.getDate()); //TODO time zone
            jPost.put("postLat", post.getLat());
            jPost.put("postLon", post.getLon());
            if (post.getPicture() != null){
                jPost.put("postPictureId", post.getPicture().getId());
                jPost.put("postPictureWebPath", post.getPicture().getWebPath());
            } else {
                jPost.put("postPictureId", "null");
                jPost.put("postPictureWebPath", "null");
            }
            jPost.put("postUserID", post.getUser().getId());
            jPost.put("postUserLogin", post.getUser().getLogin());
            jPost.put("postUserFirstName", post.getUser().getFirstName());
            jPost.put("postUserSecondName", post.getUser().getSecondName());
            jPost.put("postUserAvatarId", post.getUser().getAvatar().getId());
            jPost.put("postUserAvatarWebPath", post.getUser().getAvatar().getWebPath());
            jPosts.append("posts", jPost);
        }
        jPosts.put("nextPage", nextPage);
        jPosts.put("prevPage", prevPage);
        return jPosts;
    }

    public Post addPost(Post post) throws PostActionException {
        try {
            return postDao.insert(post);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new PostActionException(String.format("{\"error\": \"%1$s\"",
                    bundle.getString("back.unknownException")));
        }
    }

    public List<Post> getFollowUsersPosts(String login, int offset, int count) throws PostActionException {
        try {
            return postDao.getFollowUsersPosts(login, offset, count + 1);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new PostActionException(String.format("{\"error\": \"%1$s\"",
                    bundle.getString("back.unknownException")));
        }
    }

    @SuppressWarnings("Duplicates")
    public JSONObject getJsonFollowUsersPost(String login, int offset, int count) throws PostActionException {
        List<Post> posts = getFollowUsersPosts(login, offset, count);
        boolean nextPage = posts.size() > count;
        boolean prevPage = offset != 0;

        JSONObject jPosts = new JSONObject();
        JSONObject jPost;
        for(Post post : posts){
            jPost = new JSONObject();
            jPost.put("postId", post.getId());
            jPost.put("postText", post.getText());
            jPost.put("postDate", post.getDate()); //TODO time zone
            jPost.put("postLat", post.getLat());
            jPost.put("postLon", post.getLon());
            if (post.getPicture() != null){
                jPost.put("postPictureId", post.getPicture().getId());
                jPost.put("postPictureWebPath", post.getPicture().getWebPath());
            } else {
                jPost.put("postPictureId", "null");
                jPost.put("postPictureWebPath", "null");
            }
            jPost.put("postUserID", post.getUser().getId());
            jPost.put("postUserLogin", post.getUser().getLogin());
            jPost.put("postUserFirstName", post.getUser().getFirstName());
            jPost.put("postUserSecondName", post.getUser().getSecondName());
            jPost.put("postUserAvatarId", post.getUser().getAvatar().getId());
            jPost.put("postUserAvatarWebPath", post.getUser().getAvatar().getWebPath());
            jPosts.append("posts", jPost);
        }
        jPosts.put("nextPage", nextPage);
        jPosts.put("prevPage", prevPage);
        return jPosts;
    }

    public void deletePost(long id) throws PostActionException {
        try {
            postDao.delete(id);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new PostActionException("{}");
        }
    }
}
