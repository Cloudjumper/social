package org.drekorik.social.dbinter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

// https://tomcat.apache.org/tomcat-8.0-doc/jdbc-pool.html
public class MySqlDBConnectionPool {
    private static final Logger LOGGER = LogManager.getLogger("FASTD");
    private static final DataSource DATASOURCE = new DataSource();
    static {
        Properties properties = new Properties();
        try(InputStream inputStream = MySqlDBConnectionPool.class.getResourceAsStream("/MySqlDB.properties")) {
            properties.load(inputStream);

            PoolProperties p = new PoolProperties();
            p.setUrl(properties.getProperty("mysql.url"));
            p.setDriverClassName("com.mysql.cj.jdbc.Driver");
            p.setUsername(properties.getProperty("mysql.user"));
            p.setPassword(properties.getProperty("mysql.pass"));
//          (boolean) Register the pool with JMX or not. The default value is true.
            p.setJmxEnabled(true);
            p.setTestWhileIdle(false);
            p.setTestOnBorrow(true);
            p.setValidationQuery("SELECT 1");
            p.setTestOnReturn(false);
//          (long) avoid excess validation, only run validation at most at this frequency - time in milliseconds.
//          If a connection is due for validation, but has been validated previously within this interval,
//          it will not be validated again. The default value is 3000 (3 seconds).
            p.setValidationInterval(30000);
//          (int) The number of milliseconds to sleep between runs of the idle connection validation/cleaner thread.
//          This value should not be set under 1 second. It dictates how often we check for idle, abandoned connections,
//          and how often we validate idle connections.
            p.setTimeBetweenEvictionRunsMillis(30000);
            p.setMaxActive(Integer.parseInt(properties.getProperty("mysql.maxActive")));
            p.setInitialSize(Integer.parseInt(properties.getProperty("mysql.initActive")));
            p.setMaxWait(Integer.parseInt(properties.getProperty("mysql.maxWait")));
//          (int) Timeout in seconds before an abandoned(in use) connection can be removed.
//          The default value is 60 (60 seconds).
//          The value should be set to the longest running query your applications might have.
            p.setRemoveAbandonedTimeout(60);
            p.setMinEvictableIdleTimeMillis(30000);
            p.setMinIdle(Integer.parseInt(properties.getProperty("mysql.minIdle")));
            p.setLogAbandoned(true);
            p.setRemoveAbandoned(true);
//          Predefined interceptors:
//          org.apache.tomcat.jdbc.pool.interceptor.
//                  ConnectionState - keeps track of auto commit, read only, catalog and transaction isolation level.
//          org.apache.tomcat.jdbc.pool.interceptor.
//                  StatementFinalizer - keeps track of opened statements, and closes them when the connection is returned to the pool.
            p.setJdbcInterceptors(
                    "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
                            "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
            DATASOURCE.setPoolProperties(p);
        } catch (IOException e) {
            LOGGER.error("Reading resource file exception");
            LOGGER.error(e.getMessage());
        }
    }

    public static Connection getConnection() throws SQLException {
        return DATASOURCE.getConnection();
    }
}
