package org.drekorik.social.controllers.page;

import org.drekorik.social.exceptions.UserActionsException;
import org.drekorik.social.logic.UserActions;
import org.drekorik.social.models.User;
import org.drekorik.social.utils.SubstringFinder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

public class UserPage extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserActions userActions = new UserActions((Locale) req.getSession().getAttribute("locale"));
        String login = SubstringFinder.find(req.getRequestURI(), "(?<=/user/)[a-zA-Z0-9]+$");
        User sessionUser = ((User) req.getSession().getAttribute("sUser"));
        User user;
        //  It's "lazy" AND, so there will not be NPE
        if (login != null && !login.equals("")){
            try {
                user = userActions.get(login);
                if (user == null) {
                    resp.sendError(404);
                    return;
                }
            } catch (UserActionsException e) {
                resp.sendError(400);
                return;
            }
        }
        else if (req.getSession().getAttribute("sUser") != null){
            resp.sendRedirect(req.getContextPath() + String.format("/user/%1$s", sessionUser.getLogin()));
            return;
        }
        else {
            resp.sendRedirect(req.getContextPath() + "/registration");
            return;
        }
        if (req.getSession().getAttribute("sUser") != null && sessionUser.getLogin().equals(login)) {
            req.setAttribute("owner", true);
            try {
                req.setAttribute("subs", userActions.subscriptions(sessionUser));
            } catch (UserActionsException e) {
                resp.sendError(404);
                return;
            }
        }
        req.setAttribute("user", user);
        req.getRequestDispatcher("/WEB-INF/views/user.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
