package org.drekorik.social.controllers.page;

import org.drekorik.social.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cloudjumper on 12/21/16.
 */
public class IndexPage extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("sUser") != null){
            User user = (User) req.getSession().getAttribute("sUser");
            resp.sendRedirect(String.format("/user/%1$s", user.getLogin()));
        }else {
            resp.sendRedirect(req.getContextPath() + "/registration");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
