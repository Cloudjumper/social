package org.drekorik.social.controllers.page;

import org.drekorik.social.exceptions.UserActionsException;
import org.drekorik.social.logic.UserActions;
import org.drekorik.social.models.mod.SearchUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SearchPage extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("sUser") == null) {
            resp.sendRedirect(req.getContextPath() + "/registration");
            return;
        }
        String search = req.getParameter("q");
        List<SearchUser> users = new ArrayList<>();
        if (search != null){
            UserActions userActions = new UserActions((Locale) req.getSession().getAttribute("locale"));
            try {
                users = userActions.findUsers(search);
            } catch (UserActionsException e) {
                resp.sendError(404);
                return;
            }
        }
        req.setAttribute("users", users);
        req.getRequestDispatcher("/WEB-INF/views/search.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
