package org.drekorik.social.controllers.page;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cloudjumper on 3/16/17.
 */
public class InfoChangePage extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("sUser") == null)
            req.getRequestDispatcher("/WEB-INF/views/registration.jsp").forward(req, resp);
        else{
            req.setAttribute("oldUser", req.getSession().getAttribute("sUser"));
            req.getRequestDispatcher("/WEB-INF/views/userInfoChange.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
