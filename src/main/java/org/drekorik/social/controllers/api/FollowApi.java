package org.drekorik.social.controllers.api;

import org.drekorik.social.exceptions.UserActionsException;
import org.drekorik.social.logic.UserActions;
import org.drekorik.social.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

public class FollowApi extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserActions userActions = new UserActions((Locale) req.getSession().getAttribute("locale"));
        User user2 = new User();
        user2.setLogin(req.getParameter("u"));
        User user1 = (User) req.getSession().getAttribute("sUser");
        try {
            userActions.follow(user1, user2);
        } catch (UserActionsException e) {
            resp.setStatus(400);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserActions userActions = new UserActions((Locale) req.getSession().getAttribute("locale"));
        User user2 = new User();
        user2.setLogin(req.getParameter("u"));
        User user1 = (User) req.getSession().getAttribute("sUser");
        try {
            userActions.stopFollow(user1, user2);
        } catch (UserActionsException e) {
            resp.setStatus(400);
        }
    }
}
