package org.drekorik.social.controllers.api;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

public class LangChangeApi extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("lang") != null){
            req.getSession().setAttribute("locale", new Locale(req.getParameter("lang")));
            req.getSession().setAttribute("lang", req.getParameter("lang"));
        }
        else
            resp.setStatus(400);
    }
}
