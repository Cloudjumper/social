package org.drekorik.social.controllers.api;

import org.drekorik.social.exceptions.UserActionsException;
import org.drekorik.social.logic.UserActions;
import org.drekorik.social.models.User;
import org.json.JSONObject;
import org.json.JSONTokener;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

public class InfoChangeApi extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserActions userActions = new UserActions((Locale) req.getSession().getAttribute("locale"));
        try(BufferedReader reader = req.getReader();
            PrintWriter writer = resp.getWriter()) {
            try {
                JSONObject input = new JSONObject(new JSONTokener(reader));
                User oldUser = (User) req.getSession().getAttribute("sUser");
                userActions.update(input, oldUser);
                req.getSession().setAttribute("sUser", userActions.get(oldUser.getLogin()));
                writer.println("{}");
            } catch (UserActionsException e) {
                resp.setStatus(400);
                writer.printf(e.getMessage());
            }
        }
    }
}
