package org.drekorik.social.controllers.api;

import org.drekorik.social.exceptions.PostActionException;
import org.drekorik.social.logic.PostActions;
import org.drekorik.social.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

public class NewsApi extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User sUser = (User) req.getSession().getAttribute("sUser");
        if (sUser == null){
            resp.setStatus(400);
            return;
        }
        try(PrintWriter writer = resp.getWriter()) {
            PostActions postActions = new PostActions((Locale) req.getSession().getAttribute("locale"));
            int page = Integer.parseInt(req.getParameter("p"));
            int count = 5;
            int offset = (page - 1) * count;
            resp.setContentType("application/json");
            resp.addCookie(new Cookie("page", Integer.toString(page)));
            writer.println(postActions.getJsonFollowUsersPost(sUser.getLogin(), offset, count).toString());
        } catch (PostActionException e) {
            e.printStackTrace();
        }
    }
}
