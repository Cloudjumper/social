package org.drekorik.social.controllers.api;

import org.drekorik.social.exceptions.PictureActionsException;
import org.drekorik.social.exceptions.PostActionException;
import org.drekorik.social.logic.PictureActions;
import org.drekorik.social.logic.PostActions;
import org.drekorik.social.models.Picture;
import org.drekorik.social.models.Post;
import org.drekorik.social.models.User;
import org.drekorik.social.utils.TimeUtil;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.Locale;

@MultipartConfig
public class PostApi extends HttpServlet {
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PostActions postActions = new PostActions((Locale) req.getSession().getAttribute("locale"));
        if (req.getParameter("q") != null){
            try {
                postActions.deletePost(Long.parseLong(req.getParameter("q")));
            } catch (PostActionException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("u");
        User sUser = (User) req.getSession().getAttribute("sUser");
        if (login == null || login.equals("")){
            resp.setStatus(400);
            return;
        }
        try(PrintWriter writer = resp.getWriter()) {
            PostActions postActions = new PostActions((Locale) req.getSession().getAttribute("locale"));
            int page = Integer.parseInt(req.getParameter("p"));
            int count = 5;
            int offset = (page - 1) * count;
            resp.setContentType("application/json");
            JSONObject result = postActions.getJsonUsersPosts(login, offset, count);
            result.put("owner", sUser != null && sUser.getLogin().equals(login));
            writer.println(result.toString());
        } catch (PostActionException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part filePart = req.getPart("pic");
        try {
            Picture picture = null;
            if (filePart !=null){
                PictureActions pictureActions = new PictureActions((Locale) req.getSession().getAttribute("locale"));
                picture = pictureActions.save(filePart);
            }
            double lat = Double.parseDouble(req.getParameter("lat"));
            double lon = Double.parseDouble(req.getParameter("lon"));

            User user = (User) req.getSession().getAttribute("sUser");
            Post post = new Post();
            post.setText(req.getParameter("text"));
            post.setPicture(picture);
            post.setDate(TimeUtil.timeAtUTC(LocalDateTime.now()));
            post.setUser(user);
            post.setLat(lat);
            post.setLon(lon);
            PostActions postActions = new PostActions((Locale) req.getSession().getAttribute("locale"));
            postActions.addPost(post);
        } catch (PictureActionsException | PostActionException e) {
            resp.setStatus(400);
            try(PrintWriter writer = resp.getWriter()) {
                writer.printf(e.getMessage());
            }
        }
    }
}
