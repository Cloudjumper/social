package org.drekorik.social.controllers.api;

import org.drekorik.social.exceptions.PictureActionsException;
import org.drekorik.social.exceptions.UserActionsException;
import org.drekorik.social.logic.PictureActions;
import org.drekorik.social.logic.UserActions;
import org.drekorik.social.models.Picture;
import org.drekorik.social.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

@MultipartConfig(maxFileSize = 10_485_760)
public class AvatarChangeApi extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part filePart = req.getPart("pic"); // Retrieves <input type="file" name="file">
        try {
            PictureActions pictureActions = new PictureActions((Locale) req.getSession().getAttribute("locale"));
            Picture picture = pictureActions.save(filePart);

            User sessionUser = (User) req.getSession().getAttribute("sUser");
            sessionUser.setAvatar(picture);
            UserActions userActions = new UserActions((Locale) req.getSession().getAttribute("locale"));
            userActions.avatarChange(sessionUser);
            req.getSession().setAttribute("sUser", sessionUser);
        } catch (PictureActionsException | UserActionsException e) {
            resp.setStatus(400);
            try(PrintWriter writer = resp.getWriter()) {
                writer.printf(e.getMessage());
            }
        }
    }
}
