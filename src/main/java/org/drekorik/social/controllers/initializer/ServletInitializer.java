package org.drekorik.social.controllers.initializer;

import org.drekorik.social.controllers.api.*;
import org.drekorik.social.controllers.page.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Created by cloudjumper on 3/4/17.
 */
@WebListener
public class ServletInitializer implements ServletContextListener {

    public void contextInitialized(ServletContextEvent event) {
        ServletContext servletContext = event.getServletContext();
        servletContext.addServlet("registrationPage", RegistrationPage.class)
                .addMapping("/registration");
        servletContext.addServlet("userPage", UserPage.class)
                .addMapping("/user", "/user/*");
        servletContext.addServlet("newsPage", NewsPage.class)
                .addMapping("/news");
        servletContext.addServlet("searchPage", SearchPage.class)
                .addMapping("/search");
        servletContext.addServlet("infoChangePage", InfoChangePage.class)
                .addMapping("/change");
        servletContext.addServlet("indexPage", IndexPage.class)
                .addMapping("/");

        servletContext.addServlet("registrationApi", RegistrationApi.class)
                .addMapping("/api/register");
        servletContext.addServlet("newsApi", NewsApi.class)
                .addMapping("/api/news");
        servletContext.addServlet("loginApi", LogInApi.class)
                .addMapping("/api/auth");
        servletContext.addServlet("langApi", LangChangeApi.class)
                .addMapping("/api/lang");
        servletContext.addServlet("followApi", FollowApi.class)
                .addMapping("/api/follow");
        servletContext.addServlet("avatarApi", AvatarChangeApi.class)
                .addMapping("/api/avatar");
        servletContext.addServlet("postApi", PostApi.class)
                .addMapping("/api/post");
        servletContext.addServlet("infoChangeApi", InfoChangeApi.class)
                .addMapping("/api/info-change");

        servletContext.getServletRegistration("default")
                .addMapping("*.js", "*.css", "*.png", "*.jpg", "*.jpeg", "*.ico");
    }
}
