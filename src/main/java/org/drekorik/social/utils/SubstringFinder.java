package org.drekorik.social.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by cloudjumper on 3/13/17.
 */
public interface SubstringFinder {
    static String find(String string, String pattern){
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(string);
        if (m.find()){
            return m.group(0);
        }
        return null;
    }
}
