package org.drekorik.social.utils;

import org.mindrot.jbcrypt.BCrypt;

import java.util.Base64;
import java.util.ResourceBundle;

/**
 * Created by cloudjumper on 3/1/17.
 */
public class Hashing {
    // $ - delimiter, 2a - algorithm, 10 - rounds, other - salt
    //                                  length|              29s            |
    private static final String SALT        = "$2a$10$pYXthssX31HRB9ZpbVIWLe";
    private static final String FAST_SALT   = "$2a$04$oefTgtbOMNtg3vnhtscXbO";

    public static String hash(String pass) {
        return BCrypt.hashpw(pass, SALT).substring(29);
    }

    @Deprecated
    public static String hash(String pass, String salt) {
        return BCrypt.hashpw(pass, salt).substring(29);
    }

    public static String fastHash(String string){
        return BCrypt.hashpw(string, FAST_SALT).substring(29);
    }

    public static String fastRandomHash(String string){
        return BCrypt.hashpw(string, genSalt(4)).substring(29);
    }

    public static String toBase64(String string){
        return new String(Base64.getEncoder().encode(string.getBytes())).intern();
    }

    @SuppressWarnings("WeakerAccess")
    public static boolean check(String pass, String hash) {
        return BCrypt.checkpw(pass, SALT + hash);
    }

    @Deprecated
    public static boolean check(String pass, String hash, String salt) {
        return BCrypt.checkpw(pass, salt + hash);
    }

    @SuppressWarnings("WeakerAccess")
    public static String genSalt(int r) {
        return BCrypt.gensalt(r);
    }
}
