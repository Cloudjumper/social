package org.drekorik.social.utils;

import org.drekorik.social.exceptions.LocalDateTimeParseException;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public interface TimeUtil {
    /**
     * Change {@link LocalDateTime} time zone via changing time, not zone
     * @param time
     * @param zone
     * @return
     */
    static LocalDateTime timeAtZone(LocalDateTime time, String zone){
        return time.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of(zone)).toLocalDateTime();
    }

    /**
     * Convert {@link ZonedDateTime} to {@link LocalDateTime} with changing time zone via changing time, not zone
     * @param time
     * @param zone
     * @return
     */
    static LocalDateTime timeAtZone(ZonedDateTime time, String zone){
        return time.withZoneSameInstant(ZoneId.of(zone)).toLocalDateTime();
    }

    /**
     * Get {@link LocalDateTime} with time at UTC
     * @param time
     * @return
     */
    static LocalDateTime timeAtUTC(LocalDateTime time){
        return time.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("+00:00:00")).toLocalDateTime();
    }

    /**
     * Get {@link LocalDateTime} with time at UTC
     * @param time
     * @return
     */
    static LocalDateTime timeAtUTC(ZonedDateTime time){
        return time.withZoneSameInstant(ZoneId.of("+00:00:00")).toLocalDateTime();
    }

    /**
     * Convert {@link ZonedDateTime} to print string in format dd-MM-yyyy HH:mm
     * @param time
     * @return
     */
    static String timeToString(ZonedDateTime time){
        return time.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
    }

    /**
     * Convert {@link LocalDateTime} to print string in format dd-MM-yyyy HH:mm
     * @param time
     * @return
     */
    static String timeToString(LocalDateTime time){
        return time.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
    }

    /**
     * Convert {@link LocalDate} to print string in format dd-MM-yyyy
     * @param date
     * @return
     */
    static String dateToString(LocalDate date){
        return date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    /**
     * Convert {@link LocalDateTime} to unix time stamp, zone offset +00:00:00
     * @param time
     * @return
     */
    static long toUnixTimeStamp(LocalDateTime time){
        return time.toEpochSecond(ZoneOffset.of("+00:00:00"));
    }

    /**
     * Convert {@link ZonedDateTime} to unix time stamp, zone offset +00:00:00
     * @param time
     * @return
     */
    static long toUnixTimeStamp(ZonedDateTime time){
        return toUnixTimeStamp(time.toLocalDateTime());
    }

    static LocalDateTime parse(String time) throws LocalDateTimeParseException {
        DateTimeFormatter[] patterns = {
                DateTimeFormatter.ofPattern("yyyy MM dd HH mm"),
                DateTimeFormatter.ofPattern("dd MM yyyy HH mm"),
                DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"),
                DateTimeFormatter.ofPattern("y-M-d-H-m"),
                DateTimeFormatter.ofPattern("y/M/d-H:m"),
                DateTimeFormatter.ISO_DATE_TIME
        };
        for (DateTimeFormatter d: patterns) {
            try{
                return LocalDateTime.parse(time, d);
            } catch (NullPointerException e){
                throw new LocalDateTimeParseException("null");
            } catch (DateTimeParseException ignored){}

        }
        throw new LocalDateTimeParseException("Parse error");
    }
}
