package org.drekorik.social.dao;

import org.drekorik.social.models.User;
import org.drekorik.social.models.mod.SearchUser;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by cloudjumper on 3/14/17.
 */
public interface UserDao {
    User insert(User user) throws SQLException;
    User select(String login) throws SQLException;
    List<User> selectAll() throws SQLException;
    void update(User user) throws SQLException;
    void delete(User user) throws SQLException;

    boolean logInCheck(String login, String pass) throws SQLException;
    int duplicateCheck(String login, String email) throws SQLException;
    void avatarChange(User user) throws SQLException;
    List<SearchUser> findUser(String login) throws SQLException;
    void follow(User user1, User user2) throws SQLException;
    void stopFollow(User user1, User user2) throws SQLException;
    List<User> subscription(User user) throws SQLException;
}
