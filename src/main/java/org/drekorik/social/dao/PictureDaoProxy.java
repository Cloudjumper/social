package org.drekorik.social.dao;

import org.drekorik.social.models.Picture;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by cloudjumper on 3/18/17.
 */
public class PictureDaoProxy implements PictureDao {
    private static volatile PictureDao instance;
    private PictureDao realPictureDao;

    public static PictureDao getInstance() {
        PictureDao localInstance = instance;
        if (localInstance == null) {
            synchronized (PostDaoProxy.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new PictureDaoProxy();
                }
            }
        }
        return localInstance;
    }

    private PictureDaoProxy() {
        realPictureDao = new PictureDaoMySql();
    }

    @Override
    public Picture insert(Picture picture) throws SQLException {
        return realPictureDao.insert(picture);
    }

    @Override
    public Picture select(String id) throws SQLException {
        return realPictureDao.select(id);
    }

    @Override
    public List<Picture> selectAll() throws SQLException {
        return realPictureDao.selectAll();
    }

    @Override
    public void update(Picture picture) throws SQLException {
        realPictureDao.update(picture);
    }

    @Override
    public void delete(Picture picture) throws SQLException {
        realPictureDao.delete(picture);
    }
}
