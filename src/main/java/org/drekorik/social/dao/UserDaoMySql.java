package org.drekorik.social.dao;

import org.drekorik.social.dbinter.MySqlDBConnectionPool;
import org.drekorik.social.models.Picture;
import org.drekorik.social.models.User;
import org.drekorik.social.models.mod.SearchUser;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

class UserDaoMySql implements UserDao {
    private static final String LOG_IN_CHECK = "SELECT count(id) AS exist FROM User WHERE login = ? AND pass = ?";
    private static final String NEW_USER_CHECK = "SELECT " +
            "(SELECT count(id) FROM User WHERE login = ?) AS login_exist, " +
            "(SELECT count(id) FROM User WHERE email = ?) AS email_exist";
    private static final String INSERT_USER = "INSERT INTO User " +
            "(login, pass, email, first_name, second_name, birth_date, zone)" +
            "VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String SELECT_USER = "SELECT User.id,User.login, User.email, User.first_name, " +
            "User.second_name, User.birth_date, User.zone, User.avatar, Picture.web_path FROM User " +
            "INNER JOIN Picture ON User.avatar = Picture.id " +
            "WHERE User.login=? ORDER BY birth_date";
    private static final String UPDATE_AVATAR = "UPDATE User SET User.avatar=? WHERE User.login = ?";
    private static final String UPDATE_USER = "UPDATE User SET " +
            "User.email=?, User.first_name=?, User.second_name=?, User.birth_date=?, User.zone=? " +
            "WHERE User.login = ?";

    private static final String SELECT_USER_LIKE_PATTERN =
            "SELECT\n" +
            "  User.id,\n" +
            "  User.login,\n" +
            "  User.email,\n" +
            "  User.first_name,\n" +
            "  User.second_name,\n" +
            "  User.birth_date,\n" +
            "  User.zone,\n" +
            "  Picture.id AS 'picture_id',\n" +
            "  Picture.web_path,\n" +
            "  Picture.file_path,\n" +
            "  follower_user_id,\n" +
            "  follow_user_id\n" +
            "FROM User\n" +
            "  LEFT JOIN Picture ON User.avatar = Picture.id\n" +
            "  LEFT JOIN Follower ON User.id = Follower.follow_user_id\n" +
            "WHERE login LIKE ?;";

    private static final String FOLLOW = "INSERT INTO Follower (follower_user_id, follow_user_id) " +
            "VALUES ((SELECT id FROM User WHERE login = ?), (SELECT id FROM User WHERE login = ?))";
    private static final String STOP_FOLLOW = "DELETE Follower FROM Follower " +
            "WHERE follower_user_id = (SELECT id FROM User WHERE login = ?) " +
            "AND follow_user_id = (SELECT id FROM User WHERE login = ?)";

    private static final String SUBSCRIPTIONS =
            "SELECT\n" +
            "  User.id,\n" +
            "  User.login,\n" +
            "  User.email,\n" +
            "  User.first_name,\n" +
            "  User.second_name,\n" +
            "  User.birth_date,\n" +
            "  User.zone,\n" +
            "  User.avatar,\n" +
            "  Picture.web_path,\n" +
            "  Picture.file_path\n" +
            "FROM Follower\n" +
            "  LEFT JOIN User ON Follower.follow_user_id = User.id\n" +
            "  LEFT JOIN Picture ON User.avatar = Picture.id\n" +
            "WHERE follower_user_id = (SELECT User.id\n" +
            "                          FROM User\n" +
            "                          WHERE login = ?);";

    @Override
    public List<User> selectAll() throws SQLException {
        return null;
    }

    @Override
    public void delete(User user) {
    }

    @Override
    public User insert(User user) throws SQLException {
        try (Connection connection = MySqlDBConnectionPool.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, user.getLogin().intern());
            preparedStatement.setString(2, user.getPass().intern());
            preparedStatement.setString(3, user.getEmail().intern());
            preparedStatement.setString(4, user.getFirstName().intern());
            preparedStatement.setString(5, user.getSecondName().intern());
            preparedStatement.setDate(6, Date.valueOf(user.getBirthDate()));
            preparedStatement.setString(7, user.getZone().intern());
            preparedStatement.executeUpdate();
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next())
                    user.setId(generatedKeys.getLong(1));
                return user;
            }
        }
    }

    @Override
    public void update(User user) throws SQLException {
        try (Connection connection = MySqlDBConnectionPool.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(UPDATE_USER)) {
            preparedStatement.setString(1, user.getEmail().intern());
            preparedStatement.setString(2, user.getFirstName().intern());
            preparedStatement.setString(3, user.getSecondName().intern());
            preparedStatement.setDate(4, Date.valueOf(user.getBirthDate()));
            preparedStatement.setString(5, user.getZone().intern());
            preparedStatement.setString(6, user.getLogin().intern());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public User select(String login) throws SQLException {
        try (
                Connection connection = MySqlDBConnectionPool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER)
        ) {
            preparedStatement.setString(1, login);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    return new User(rs.getLong("id"), rs.getString("login").intern(), "",
                            rs.getString("email").intern(), rs.getString("first_name").intern(),
                            rs.getString("second_name").intern(), rs.getDate("birth_date").toLocalDate(),
                            rs.getString("zone").intern(),
                            new Picture(rs.getString("avatar").intern(),
                                    rs.getString("web_path").intern(), "")
                    );
                } else return null;
            }
        }
    }

    @Override
    public boolean logInCheck(String login, String pass) throws SQLException {
        try (
                Connection connection = MySqlDBConnectionPool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(LOG_IN_CHECK)
        ) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, pass);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                rs.next();
                return rs.getInt("exist") == 1;
            }
        }
    }

    /**
     * Check for login and email matches in db
     *
     * @return 0 if no matches, 1 if login exist, 2 if email exists, 3 if both exists
     */
    @Override
    public int duplicateCheck(String login, String email) throws SQLException {
        try (
                Connection connection = MySqlDBConnectionPool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(NEW_USER_CHECK)
        ) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, email);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                rs.next();
                return rs.getInt("login_exist") + (rs.getInt("email_exist") * 2);
            }
        }
    }

    @Override
    public void avatarChange(User user) throws SQLException {
        try (Connection connection = MySqlDBConnectionPool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_AVATAR)) {
            preparedStatement.setString(1, user.getAvatar().getId());
            preparedStatement.setString(2, user.getLogin());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public List<SearchUser> findUser(String login) throws SQLException {
        try (Connection connection = MySqlDBConnectionPool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_LIKE_PATTERN)) {
            preparedStatement.setString(1, "%" + login + "%");
            try (ResultSet rs = preparedStatement.executeQuery()) {
                List<SearchUser> users = new ArrayList<>();
                while (rs.next()) {
                    users.add(new SearchUser(rs.getLong("id"), rs.getString("login").intern(), "",
                            rs.getString("email").intern(), rs.getString("first_name").intern(),
                            rs.getString("second_name").intern(), rs.getDate("birth_date").toLocalDate(),
                            rs.getString("zone").intern(),
                            new Picture(rs.getString("picture_id").intern(),
                                    rs.getString("web_path").intern(),
                                    rs.getString("file_path").intern()),
                            rs.getObject("follow_user_id") != null));
                }
                return users;
            }
        }
    }

    @Override
    public void follow(User user1, User user2) throws SQLException {
        try (
                Connection connection = MySqlDBConnectionPool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(FOLLOW)
        ) {
            preparedStatement.setString(1, user1.getLogin().intern());
            preparedStatement.setString(2, user2.getLogin().intern());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void stopFollow(User user1, User user2) throws SQLException {
        try (
                Connection connection = MySqlDBConnectionPool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(STOP_FOLLOW)
        ) {
            preparedStatement.setString(1, user1.getLogin().intern());
            preparedStatement.setString(2, user2.getLogin().intern());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public List<User> subscription(User user) throws SQLException {
        try(Connection connection = MySqlDBConnectionPool.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SUBSCRIPTIONS)) {
            preparedStatement.setString(1,user.getLogin());
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                List<User> users = new ArrayList<>();
                while (resultSet.next()){
                    users.add(new User(resultSet.getLong("id"),
                            resultSet.getString("login").intern(), "",
                            resultSet.getString("email").intern(),
                            resultSet.getString("first_name").intern(),
                            resultSet.getString("second_name").intern(),
                            resultSet.getDate("birth_date").toLocalDate(),
                            resultSet.getString("zone").intern(),
                            new Picture(resultSet.getString("avatar").intern(),
                                    resultSet.getString("web_path").intern(),
                                    resultSet.getString("file_path").intern())
                            )
                    );
                }
                return users;
            }
        }
    }
}

