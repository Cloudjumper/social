package org.drekorik.social.dao;

import org.drekorik.social.models.Post;

import java.sql.SQLException;
import java.util.List;


public class PostDaoProxy implements PostDao {
    private static volatile PostDao instance;
    private PostDao realPostDao;

    public static PostDao getInstance() {
        PostDao localInstance = instance;
        if (localInstance == null) {
            synchronized (PostDaoProxy.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new PostDaoProxy();
                }
            }
        }
        return localInstance;
    }

    private PostDaoProxy() {
        realPostDao = new PostDaoMySql();
    }


    @Override
    public Post insert(Post post) throws SQLException {
        return realPostDao.insert(post);
    }

    @Override
    public Post select(long id) throws SQLException {
        return realPostDao.select(id);
    }

    @Override
    public List<Post> selectAll() throws SQLException {
        return realPostDao.selectAll();
    }

    @Override
    public void update(Post post) throws SQLException {
        realPostDao.update(post);
    }

    @Override
    public void delete(long id) throws SQLException {
        realPostDao.delete(id);
    }

    @Override
    public List<Post> getFollowUsersPosts(String login, int from, int to) throws SQLException {
        return realPostDao.getFollowUsersPosts(login, from, to);
    }

    @Override
    public List<Post> getPosts(String login, int offset, int count) throws SQLException {
        return realPostDao.getPosts(login, offset, count);
    }
}
