package org.drekorik.social.dao;

import org.drekorik.social.dbinter.MySqlDBConnectionPool;
import org.drekorik.social.models.Picture;
import org.drekorik.social.models.Post;
import org.drekorik.social.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

class PostDaoMySql implements PostDao {
    private static final String SELECT_USER_POSTS =
            "SELECT\n" +
            "  post_id,\n" +
            "  post_text,\n" +
            "  post_date,\n" +
            "  post_user_id,\n" +
            "  post_lat,\n" +
            "  post_lon,\n" +
            "  post_picture_id,\n" +
            "  post_picture_web_path,\n" +
            "  post_picture_file_path,\n" +
            "  User.login        AS 'post_user_login',\n" +
            "  User.email        AS 'post_user_email',\n" +
            "  User.first_name   AS 'post_user_first_name',\n" +
            "  User.second_name  AS 'post_user_second_name',\n" +
            "  User.birth_date   AS 'post_user_birth_date',\n" +
            "  User.zone         AS 'post_user_zone',\n" +
            "  User.avatar       AS 'post_user_avatar_id',\n" +
            "  Picture.web_path  AS 'post_user_avatar_web_path',\n" +
            "  Picture.file_path AS 'post_user_avatar_file_path'\n" +
            "FROM (SELECT\n" +
            "        Post.id           AS 'post_id',\n" +
            "        Post.text         AS 'post_text',\n" +
            "        Post.date         AS 'post_date',\n" +
            "        Post.user_id      AS 'post_user_id',\n" +
            "        Post.lat          AS 'post_lat',\n" +
            "        Post.lon          AS 'post_lon',\n" +
            "        Picture.id        AS 'post_picture_id',\n" +
            "        Picture.web_path  AS 'post_picture_web_path',\n" +
            "        Picture.file_path AS 'post_picture_file_path'\n" +
            "      FROM Post\n" +
            "        INNER JOIN User ON Post.user_id = User.id\n" +
            "        LEFT JOIN Picture ON Post.picture_id = Picture.id\n" +
            "      WHERE Post.user_id = (SELECT User.id\n" +
            "                            FROM User\n" +
            "                            WHERE login = ?)\n" +
            "     ) AS post\n" +
            "  INNER JOIN User ON User.id = post_user_id\n" +
            "  LEFT JOIN Picture ON Picture.id = User.avatar\n" +
            "ORDER BY post_date DESC\n" +
            "LIMIT ?, ?;";

    private static final String INSERT =
            "INSERT INTO Post (user_id, text, date, lat, lon, picture_id)\n" +
                    "VALUES ((SELECT id\n" +
                    "         FROM User\n" +
                    "         WHERE login = ?), ?, ?, ?, ?, ?)";

    private static final String SELECT_FOLLOW_USERS_POSTS =
            "SELECT\n" +
            "  post_id,\n" +
            "  post_text,\n" +
            "  post_date,\n" +
            "  post_user_id,\n" +
            "  post_lat,\n" +
            "  post_lon,\n" +
            "  post_picture_id,\n" +
            "  post_picture_web_path,\n" +
            "  post_picture_file_path,\n" +
            "  User.login        AS 'post_user_login',\n" +
            "  User.email        AS 'post_user_email',\n" +
            "  User.first_name   AS 'post_user_first_name',\n" +
            "  User.second_name  AS 'post_user_second_name',\n" +
            "  User.birth_date   AS 'post_user_birth_date',\n" +
            "  User.zone         AS 'post_user_zone',\n" +
            "  User.avatar       AS 'post_user_avatar_id',\n" +
            "  Picture.web_path  AS 'post_user_avatar_web_path',\n" +
            "  Picture.file_path AS 'post_user_avatar_file_path'\n" +
            "FROM (SELECT\n" +
            "        Post.id           AS 'post_id',\n" +
            "        Post.text         AS 'post_text',\n" +
            "        Post.date         AS 'post_date',\n" +
            "        Post.user_id      AS 'post_user_id',\n" +
            "        Post.lat          AS 'post_lat',\n" +
            "        Post.lon          AS 'post_lon',\n" +
            "        Picture.id        AS 'post_picture_id',\n" +
            "        Picture.web_path  AS 'post_picture_web_path',\n" +
            "        Picture.file_path AS 'post_picture_file_path'\n" +
            "      FROM Post\n" +
            "        INNER JOIN Follower ON follower_user_id = (SELECT id\n" +
            "                                                   FROM User\n" +
            "                                                   WHERE login = ?) AND follow_user_id = user_id\n" +
            "        INNER JOIN User ON Post.user_id = User.id\n" +
            "        LEFT JOIN Picture ON Post.picture_id = Picture.id) AS post\n" +
            "  INNER JOIN User ON User.id = post_user_id\n" +
            "  LEFT JOIN Picture ON Picture.id = User.avatar\n" +
            "ORDER BY post_date DESC\n" +
            "LIMIT ?, ?;";

    private static final String DELETE_POST = "DELETE Post FROM Post WHERE id = ?";

    @Override
    public Post select(long id) throws SQLException {
        return null;
    }

    @Override
    public List<Post> selectAll() throws SQLException {
        return null;
    }

    @Override
    public void update(Post post) throws SQLException {

    }

    @Override
    public List<Post> getPosts(String login, int offset, int count) throws SQLException {
        try (Connection connection = MySqlDBConnectionPool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_POSTS)) {
            preparedStatement.setString(1, login);
            preparedStatement.setInt(2, offset);
            preparedStatement.setInt(3, count);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                List<Post> list = new ArrayList<>();
                Picture picture;
                Picture avatar;
                User user;
                while (rs.next()) {
                    if (rs.getString("post_picture_id") == null)
                        picture = null;
                    else
                        picture = new Picture(rs.getString("post_picture_id").intern(),
                                rs.getString("post_picture_web_path").intern(),
                                rs.getString("post_picture_file_path").intern());
                    avatar = new Picture(rs.getString("post_user_avatar_id").intern(),
                            rs.getString("post_user_avatar_web_path").intern(),
                            rs.getString("post_user_avatar_file_path").intern());
                    user = new User(rs.getLong("post_user_id"),
                            rs.getString("post_user_login").intern(),
                            "", rs.getString("post_user_email").intern(),
                            rs.getString("post_user_first_name").intern(),
                            rs.getString("post_user_second_name").intern(),
                            rs.getDate("post_user_birth_date").toLocalDate(),
                            rs.getString("post_user_zone").intern(), avatar);
                    list.add(new Post(rs.getLong("post_id"), user, rs.getString("post_text").intern(),
                            rs.getTimestamp("post_date").toLocalDateTime(),
                            rs.getDouble("post_lat"), rs.getDouble("post_lon"), picture));
                }
                return list;
            }
        }
    }

    @Override
    public List<Post> getFollowUsersPosts(String login, int offset, int count) throws SQLException {
        try (Connection connection = MySqlDBConnectionPool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FOLLOW_USERS_POSTS)) {
            preparedStatement.setString(1, login);
            preparedStatement.setInt(2, offset);
            preparedStatement.setInt(3, count);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                List<Post> list = new ArrayList<>();
                Picture picture;
                Picture avatar;
                User postUser;
                while (rs.next()) {
                    avatar = new Picture(rs.getString("post_user_avatar_id").intern(),
                            rs.getString("post_user_avatar_web_path").intern(),
                            rs.getString("post_user_avatar_file_path").intern());
                    postUser = new User(rs.getLong("post_user_id"),
                            rs.getString("post_user_login").intern(),
                            "", rs.getString("post_user_email").intern(),
                            rs.getString("post_user_first_name").intern(),
                            rs.getString("post_user_second_name").intern(),
                            rs.getDate("post_user_birth_date").toLocalDate(),
                            rs.getString("post_user_zone").intern(), avatar);
                    if (rs.getString("post_picture_id") == null)
                        picture = null;
                    else
                        picture = new Picture(rs.getString("post_picture_id").intern(),
                                rs.getString("post_picture_web_path").intern(),
                                "post_picture_file_path");
                    list.add(new Post(rs.getLong("post_id"), postUser,
                            rs.getString("post_text").intern(),
                            rs.getTimestamp("post_date").toLocalDateTime(),
                            rs.getDouble("post_lat"), rs.getDouble("post_lon"), picture));
                }
                return list;
            }
        }
    }

    @Override
    public Post insert(Post post) throws SQLException {
        try (Connection connection = MySqlDBConnectionPool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
            preparedStatement.setString(1, post.getUser().getLogin());
            preparedStatement.setString(2, post.getText());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(post.getDate()));
            preparedStatement.setDouble(4, post.getLat());
            preparedStatement.setDouble(5, post.getLon());
            preparedStatement.setString(6, post.getPicture() != null ? post.getPicture().getId() : null);
            preparedStatement.executeUpdate();
            return post;
        }
    }

    @Override
    public void delete(long id) throws SQLException {
        try (Connection connection = MySqlDBConnectionPool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_POST)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        }
    }
}
