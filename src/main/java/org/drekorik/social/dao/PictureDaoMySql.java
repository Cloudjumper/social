package org.drekorik.social.dao;

import org.drekorik.social.dbinter.MySqlDBConnectionPool;
import org.drekorik.social.models.Picture;

import java.sql.*;
import java.util.List;

/**
 * Created by cloudjumper on 3/9/17.
 */
public class PictureDaoMySql implements PictureDao {

    private static final String SELECT_DEFAULT_AVATAR = "SELECT id, web_path, file_path FROM Picture WHERE id = '0'";
    private static final String INSERT_PICTURE = "INSERT INTO Picture (id, web_path, file_path) " +
            "VALUES (?, ?, ?)";
    private static final String SELECT_PICTURE = "SELECT id, web_path, file_path FROM Picture WHERE id = ?";
    private static final String DELETE_PICTURE = "DELETE Picture FROM Picture WHERE id = ?";

    @Override
    public List<Picture> selectAll() throws SQLException {
        return null;
    }

    @Override
    public void update(Picture picture) throws SQLException {

    }

    @Deprecated
    public static Picture selectDefault() throws SQLException {
        try (
                Connection connection = MySqlDBConnectionPool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_DEFAULT_AVATAR);
                ResultSet rs = preparedStatement.executeQuery()
        ) {
            rs.next();
            return new Picture(rs.getString("id").intern(),
                    rs.getString("web_path").intern(), rs.getString("file_path").intern());
        }
    }

    @Override
    public Picture select(String id) throws SQLException {
        try (
                Connection connection = MySqlDBConnectionPool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PICTURE)
        ) {
            preparedStatement.setString(1, id);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (!rs.isBeforeFirst())
                    return null;
                rs.next();
                return new Picture(id,
                        rs.getString("web_path").intern(),
                        rs.getString("file_path").intern());
            }
        }
    }

    @Override
    public Picture insert(Picture picture) throws SQLException {
        try (Connection connection = MySqlDBConnectionPool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PICTURE, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, picture.getId());
            preparedStatement.setString(2, picture.getWebPath());
            preparedStatement.setString(3, picture.getFilePath());
            preparedStatement.executeUpdate();
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next())
                    picture.setId(generatedKeys.getString(1));
                return picture;
            }
        }
    }

    @Override
    public void delete(Picture picture) throws SQLException {
        try (Connection connection = MySqlDBConnectionPool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_PICTURE)) {
            preparedStatement.setString(1, picture.getId());
            preparedStatement.executeUpdate();
        }
    }
}
