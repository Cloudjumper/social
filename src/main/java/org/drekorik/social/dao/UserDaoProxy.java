package org.drekorik.social.dao;

import org.drekorik.social.models.User;
import org.drekorik.social.models.mod.SearchUser;

import java.sql.*;
import java.util.List;

public class UserDaoProxy implements UserDao {
    private static volatile UserDao instance;
    private UserDao realUserDao;

    public static UserDao getInstance() {
        UserDao localInstance = instance;
        if (localInstance == null) {
            synchronized (UserDaoProxy.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new UserDaoProxy();
                }
            }
        }
        return localInstance;
    }

    private UserDaoProxy() {
        realUserDao = new UserDaoMySql();
    }

    @Override
    public User insert(User user) throws SQLException {
        return realUserDao.insert(user);
    }

    @Override
    public User select(String login) throws SQLException {
        return realUserDao.select(login);
    }

    @Override
    public List<User> selectAll() throws SQLException {
        return realUserDao.selectAll();
    }

    @Override
    public void update(User user) throws SQLException {
        realUserDao.update(user);
    }

    @Override
    public void delete(User user) throws SQLException {
        realUserDao.delete(user);
    }

    @Override
    public boolean logInCheck(String login, String pass) throws SQLException {
        return realUserDao.logInCheck(login, pass);
    }

    @Override
    public int duplicateCheck(String login, String email) throws SQLException {
        return realUserDao.duplicateCheck(login, email);
    }

    @Override
    public void avatarChange(User user) throws SQLException {
        realUserDao.avatarChange(user);
    }

    @Override
    public List<SearchUser> findUser(String login) throws SQLException {
        return realUserDao.findUser(login);
    }

    @Override
    public void follow(User user1, User user2) throws SQLException {
        realUserDao.follow(user1, user2);
    }

    @Override
    public void stopFollow(User user1, User user2) throws SQLException {
        realUserDao.stopFollow(user1, user2);
    }

    @Override
    public List<User> subscription(User user) throws SQLException {
        return realUserDao.subscription(user);
    }
}
