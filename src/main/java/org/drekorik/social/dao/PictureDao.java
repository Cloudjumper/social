package org.drekorik.social.dao;

import org.drekorik.social.models.Picture;
import org.drekorik.social.models.User;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by cloudjumper on 3/14/17.
 */
public interface PictureDao {
    Picture insert(Picture picture) throws SQLException;
    Picture select(String id) throws SQLException;
    List<Picture> selectAll() throws SQLException;
    void update(Picture picture) throws SQLException;
    void delete(Picture picture) throws SQLException;

}
