package org.drekorik.social.dao;

import org.drekorik.social.models.Post;
import org.drekorik.social.models.User;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by cloudjumper on 3/18/17.
 */
public interface PostDao {
    Post insert(Post post) throws SQLException;
    Post select(long id) throws SQLException;
    List<Post> selectAll() throws SQLException;
    void update(Post post) throws SQLException;
    void delete(long id) throws SQLException;

    List<Post> getFollowUsersPosts(String login, int offset, int count) throws SQLException;
    List<Post> getPosts(String login, int offset, int count) throws SQLException;
}
