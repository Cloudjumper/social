DROP TABLE IF EXISTS epam_social.Galery;
DROP TABLE IF EXISTS epam_social.User;
DROP TABLE IF EXISTS epam_social.Picture;

CREATE TABLE epam_social.Picture
(
  id       VARCHAR(255) PRIMARY KEY NOT NULL,
  path     VARCHAR(255)             NOT NULL,
  web_path VARCHAR(255)             NOT NULL
);
CREATE UNIQUE INDEX Picture_id_uindex
  ON epam_social.Picture (id);

INSERT INTO Picture (id, path, web_path) 
VALUES ('0', '../webapps/ROOT/static/img/pictures/0.jpg', '/static/img/pictures/0.jpg');

CREATE TABLE epam_social.User
(
  id          INT                   AUTO_INCREMENT PRIMARY KEY,
  login       VARCHAR(255) NOT NULL,
  pass        VARCHAR(255) NOT NULL,
  email       VARCHAR(255) NOT NULL,
  first_name  VARCHAR(255),
  second_name VARCHAR(255),
  birth_date  DATE,
  zone   VARCHAR(9)                   DEFAULT 0,
  avatar      VARCHAR(255) NOT NULL DEFAULT '0',
  CONSTRAINT test_Picture_id_fk FOREIGN KEY (avatar) REFERENCES Picture (id)
);
CREATE UNIQUE INDEX User_login_uindex
  ON epam_social.User (login);
CREATE UNIQUE INDEX User_email_uindex
  ON epam_social.User (email);

INSERT INTO User (login, pass, email, first_name, second_name, birth_date, zone)
VALUES ('ngolubev', '3JLbNKtyHUVgdSBHY13EDbi8y2KnDcm', 'drekorik@gmail.com', 'Никита', 'голубев', '1993-10-14', '+03:00:00');


CREATE TABLE epam_social.Galery
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  user INT NOT NULL,
  picture VARCHAR(255) NOT NULL,
  CONSTRAINT Galery_User_id_fk FOREIGN KEY (user) REFERENCES User (id),
  CONSTRAINT Galery_Picture_id_fk FOREIGN KEY (picture) REFERENCES Picture (id)
);
CREATE UNIQUE INDEX Galery_id_uindex ON epam_social.Galery (id);