DROP TABLE IF EXISTS epam_social.User;

CREATE TABLE epam_social.User
(
  id          INT AUTO_INCREMENT PRIMARY KEY,
  login       VARCHAR(255) NOT NULL,
  pass        VARCHAR(255) NOT NULL,
  email       VARCHAR(255) NOT NULL,
  first_name  VARCHAR(255),
  second_name VARCHAR(255),
  birth_date  DATE,
  time_zone   INT DEFAULT 0
);
CREATE UNIQUE INDEX User_login_uindex
  ON epam_social.User (login);
CREATE UNIQUE INDEX User_email_uindex
  ON epam_social.User (email);

INSERT INTO User (login, pass, email, first_name, second_name, birth_date, time_zone)
VALUES ('ngolubev', SHA1('ngolubev'), 'drekorik@gmail.com', 'Nikita', 'Golubev', '1993-10-14', 3);