DROP TABLE IF EXISTS epam_social.Follower;
DROP TABLE IF EXISTS epam_social.Post;
DROP TABLE IF EXISTS epam_social.User;
DROP TABLE IF EXISTS epam_social.Picture;

CREATE TABLE epam_social.Picture
(
  id        VARCHAR(255) PRIMARY KEY NOT NULL,
  web_path  VARCHAR(255)             NOT NULL,
  file_path VARCHAR(255)             NOT NULL
);
CREATE UNIQUE INDEX Picture_id_uindex
  ON epam_social.Picture (id);

INSERT INTO Picture (id, web_path, file_path)
VALUES ('0', '/static/img/pictures/0.jpg', 'none');

CREATE TABLE epam_social.User
(
  id          BIGINT                AUTO_INCREMENT PRIMARY KEY,
  login       VARCHAR(255) NOT NULL,
  pass        VARCHAR(255) NOT NULL,
  email       VARCHAR(255) NOT NULL,
  first_name  VARCHAR(255),
  second_name VARCHAR(255),
  birth_date  DATE,
  zone        VARCHAR(9)            DEFAULT 0,
  avatar      VARCHAR(255) NOT NULL DEFAULT '0',
  CONSTRAINT User_Picture_id_fk FOREIGN KEY (avatar) REFERENCES Picture (id)
);
CREATE UNIQUE INDEX User_login_uindex
  ON epam_social.User (login);
CREATE UNIQUE INDEX User_email_uindex
  ON epam_social.User (email);

INSERT INTO User (login, pass, email, first_name, second_name, birth_date, zone)
VALUES
  ('ngolubev', '3JLbNKtyHUVgdSBHY13EDbi8y2KnDcm', 'drekorik@gmail.com', 'Никита', 'Голубев', '1993-10-14', '+03:00:00');
INSERT INTO User (login, pass, email, first_name, second_name, birth_date, zone)
VALUES ('Drekorik', '3JLbNKtyHUVgdSBHY13EDbi8y2KnDcm', 'golubev404@gmail.com', 'Nikita', 'Golubev', '1993-10-14',
        '+03:00:00');

CREATE TABLE epam_social.Post
(
  id         BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  user_id    BIGINT             NOT NULL,
  text       VARCHAR(5000)      NOT NULL,
  date       TIMESTAMP          NOT NULL,
  lat        DECIMAL(10, 6)      NOT NULL,
  lon        DECIMAL(10, 6)      NOT NULL,
  picture_id VARCHAR(255),
  CONSTRAINT Post_User_id_fk FOREIGN KEY (user_id) REFERENCES User (id),
  CONSTRAINT Post_Picture_id_fk FOREIGN KEY (picture_id) REFERENCES Picture (id)
);

CREATE TABLE epam_social.Follower
(
  follower_user_id BIGINT NOT NULL,
  follow_user_id   BIGINT NOT NULL,
  CONSTRAINT Follower_User_id_fk1 FOREIGN KEY (follow_user_id) REFERENCES User (id),
  CONSTRAINT Follower_User_id_fk2 FOREIGN KEY (follower_user_id) REFERENCES User (id)
);

INSERT INTO epam_social.Follower (follower_user_id, follow_user_id) VALUES (1, 2);
