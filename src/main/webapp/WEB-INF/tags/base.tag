<%@tag description="base" pageEncoding="UTF-8" %>
<%@attribute name="extraCss" fragment="true" %>
<%@attribute name="extraJs" fragment="true" %>
<%@attribute name="title" fragment="true" %>
<%@attribute name="content" fragment="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--<%@ taglib prefix="my" uri="http://drekorik.org/localize" %>--%>
<c:set var="lang" scope="session" value="${not empty sessionScope.lang ? sessionScope.lang : 'en'}"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="localization" var="bundle"/>
<%--<jsp:useBean id="sUser" scope="request" type="org.drekorik.social.models.User"/>--%>
<%--<jsp:setProperty name="sUser" property="${not empty sessionScope.sUser ? sessionScope.sUser : null}"/>--%>
<c:set var="sUser" scope="session" value="${not empty sessionScope.sUser ? sessionScope.sUser : null}"/>
<html>
<head>
    <title><jsp:invoke fragment="title"/></title>
    <link type="text/css" rel="stylesheet" href="/static/css/base.css">
    <link type="text/css" rel="stylesheet" href="/static/css/animate.css">
    <jsp:invoke fragment="extraCss"/>
</head>
<body>
    <div class="header">
        <div class="flex-spacer"></div>
        <div class="header-content">
            <div class="header-logo">
                <div>
                    <img width="50px" height="50px" src="/static/img/directions_car_white_54x54.png">
                </div>
                <div class="header-text">
                    <div>
                        <fmt:message key="web.base.header" bundle="${bundle}"/>
                    </div>
                </div>
            </div>
            <c:if test="${not empty sUser}">
                <div class="header-menu">
                    <div>
                        <a href="/user/${sUser.login}">
                            <img src="/static/img/home_white_54x54.png">
                        </a>
                    </div>
                    <div>
                        <a href="/news">
                            <img src="/static/img/description_white_54x54.png">
                        </a>
                    </div>
                    <div>
                        <a href="/search">
                            <img src="/static/img/search_white_54x54.png">
                        </a>
                    </div>
                    <div>
                        <a href="/change">
                            <img src="/static/img/settings_white_54x54.png">
                        </a>
                    </div>
                </div>
            </c:if>
            <div class="flex-spacer"></div>
            <div class="header-log-in">
                <div class="header-log-in-union">
                    <div class="flex-spacer"></div>
                    <div id="header-log-action" class="header-button">
                        <div>
                            <c:choose>
                                <c:when test="${not empty sUser}">
                                    ${sUser.login}
                                </c:when>
                                <c:otherwise>
                                    <fmt:message bundle="${bundle}" key="web.base.log.in"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
                <c:choose>
                    <c:when test="${not empty sUser}">
                        <div id="log-out-form" class="log-out-form">
                            <div>
                                <fmt:message scope="page" bundle="${bundle}" var="logOut" key="web.base.enter.log.out"/>
                                <input class="header-input" id="log-out" type="button" value="${logOut}">
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div id="log-in-form" class="log-in-form">
                            <div>
                                <fmt:message scope="page" bundle="${bundle}" var="login" key="web.base.enter.login"/>
                                <label for="login">${login}</label>
                                <input autocomplete="off" class="header-input" id="login" type="text" placeholder="${login}">
                            </div>
                            <div>
                                <fmt:message scope="page" bundle="${bundle}" var="pass" key="web.base.enter.password"/>
                                <label for="pass">${pass}</label>
                                <input autocomplete="off" class="header-input" id="pass" type="password" placeholder="${pass}">
                            </div>
                            <div>
                                <fmt:message scope="page" bundle="${bundle}" var="logIn" key="web.base.enter.log.in"/>
                                <input class="header-input" id="log-in" type="button" value="${logIn}">
                            </div>
                            <div id="log-in-form-err">

                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <div class="flex-spacer"></div>
    </div>
    <div class="data">
        <div class="flex-spacer">
        </div>
        <div class="data-content">
            <jsp:invoke fragment="content"/>
        </div>
        <div class="flex-spacer"></div>
    </div>
    <div class="footer">
        <div class="flex-spacer"></div>
        <div class="footer-content">
            <div class="footer-lang">
                <select id="footer-lang-select">
                    <option value="en"><fmt:message bundle="${bundle}" key="web.base.lang.en"/> </option>
                    <option selected="selected" value="ru"><fmt:message bundle="${bundle}" key="web.base.lang.ru"/> </option>
                </select>
            </div>
        </div>
        <div class="flex-spacer"></div>
    </div>
</body>
<script type="application/javascript" rel="script" src="/static/js/lib/jquery-3.1.1.min.js"></script>
<script type="application/javascript" rel="script" src="/static/js/lib/jquery-ui.min.js"></script>
<script type="application/javascript" rel="script" src="/static/js/base.js"></script>
<jsp:invoke fragment="extraJs"/>
</html>
