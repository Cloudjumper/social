#!/usr/bin/env bash
# https://github.com/less/less-plugin-autoprefix
postcss $1/*.css --use autoprefixer --no-map -d $2