<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="lang" scope="session" value="${not empty sessionScope.lang ? sessionScope.lang : 'en'}"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="localization" var="bundle"/>
<%--<jsp:useBean id="user" scope="request" type="org.drekorik.social.models.User"/>--%>
<t:base>
    <jsp:attribute name="extraCss">
        <link rel="stylesheet" type="text/css" href="/static/css/news.css">
    </jsp:attribute>
    <jsp:attribute name="extraJs">
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script rel="script" type="application/javascript" src="/static/js/news.js"></script>
    </jsp:attribute>
    <jsp:attribute name="title">
        News
    </jsp:attribute>
    <jsp:attribute name="content">
        <div id="posts">

        </div>
        <div id="navigation">
            <input disabled="disabled" id="prev" type="button" value="Back">
            <input disabled="disabled" id="next" type="button" value="Next">
        </div>
    </jsp:attribute>
</t:base>