<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="lang" scope="session" value="${not empty sessionScope.lang ? sessionScope.lang : 'en'}"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="localization" var="bundle"/>
<t:base>
    <jsp:attribute name="title">
        <fmt:message bundle="${bundle}" key="web.reg.header"/>
    </jsp:attribute>
    <jsp:attribute name="extraCss">
        <link type="text/css" rel="stylesheet" href="/static/css/registration.css">
        <link type="text/css" rel="stylesheet" href="/static/css/datetimepicker.css">
    </jsp:attribute>
    <jsp:attribute name="extraJs">
        <script type="application/javascript" rel="script" src="/static/js/registration.js"></script>
        <script type="application/javascript" rel="script" src="/static/js/lib/datetimepicker.js"></script>
        <script type="application/javascript" rel="script" src="/static/js/lib/sha1.min.js"></script>
        <%--<script type="application/javascript" rel="script" src="/static/js/datetimepicker.init.js"></script>--%>
    </jsp:attribute>
    <jsp:attribute name="content">
        <div class="wrapper">
            <div class="form">
                <div class="form-header">
                    <fmt:message bundle="${bundle}" key="web.reg.header"/>
                </div>
                <div class="form-inputs">
                    <div>
                        <fmt:message var="regLogin" bundle="${bundle}" key="web.reg.login"/>
                        <label for="reg-login">${regLogin}</label>
                        <input autocomplete="off" type="text" id="reg-login" class="reg-input" placeholder="${regLogin}" maxlength="25">
                        <div id="reg-login-err" class="reg-err"></div>
                    </div>
                    <div>
                        <fmt:message var="regPass" bundle="${bundle}" key="web.reg.pass"/>
                        <label for="reg-pass">${regPass}</label>
                        <input autocomplete="off" type="password" id="reg-pass" class="reg-input" placeholder="${regPass}" minlength="8" maxlength="25">
                        <div id="reg-pass-err" class="reg-err"></div>
                    </div>
                    <div>
                        <fmt:message var="regEmail" bundle="${bundle}" key="web.reg.email"/>
                        <label for="reg-email">${regEmail}</label>
                        <input autocomplete="off" type="text" id="reg-email" class="reg-input" placeholder="${regEmail}" maxlength="25">
                        <div id="reg-email-err" class="reg-err"></div>
                    </div>
                    <div>
                        <fmt:message var="regFirstName" bundle="${bundle}" key="web.reg.firstName"/>
                        <label for="reg-first-name">${regFirstName}</label>
                        <input autocomplete="off" type="text" id="reg-first-name" class="reg-input" placeholder="${regFirstName}" maxlength="25">
                        <div id="reg-first-name-err" class="reg-err"></div>
                    </div>
                    <div>
                        <fmt:message var="regSecondName" bundle="${bundle}" key="web.reg.secondName"/>
                        <label for="reg-second-name">${regSecondName}</label>
                        <input autocomplete="off" type="text" id="reg-second-name" class="reg-input" placeholder="${regSecondName}" maxlength="25">
                        <div id="reg-second-name-err" class="reg-err"></div>
                    </div>
                    <div>
                        <fmt:message var="regBirthDate" bundle="${bundle}" key="web.reg.birthDate"/>
                        <label for="reg-birth-date">${regBirthDate}</label>
                        <input type="text" id="reg-birth-date" class="reg-input" placeholder="${regBirthDate}">
                        <div id="reg-birth-date-err" class="reg-err"></div>
                    </div>
                    <div>
                        <fmt:message var="regTimeZone" bundle="${bundle}" key="web.reg.zone"/>
                        <label for="reg-time-zone">${regTimeZone}</label>
                        <select id="reg-time-zone" class="reg-input">
                            <option>-12:00</option>
                            <option>-11:00</option>
                            <option>-10:00</option>
                            <option>-09:30</option>
                            <option>-09:00</option>
                            <option>-08:00</option>
                            <option>-07:00</option>
                            <option>-06:00</option>
                            <option>-05:00</option>
                            <option>-04:00</option>
                            <option>-03:30</option>
                            <option>-03:00</option>
                            <option>-02:00</option>
                            <option>-01:00</option>
                            <option selected="selected">+00:00</option>
                            <option>+01:00</option>
                            <option>+02:00</option>
                            <option>+03:00</option>
                            <option>+03:30</option>
                            <option>+04:00</option>
                            <option>+04:30</option>
                            <option>+05:00</option>
                            <option>+05:30</option>
                            <option>+05:45</option>
                            <option>+06:00</option>
                            <option>+06:30</option>
                            <option>+07:00</option>
                            <option>+08:00</option>
                            <option>+08:30</option>
                            <option>+08:45</option>
                            <option>+09:00</option>
                            <option>+09:30</option>
                            <option>+10:00</option>
                            <option>+10:30</option>
                            <option>+11:00</option>
                            <option>+12:45</option>
                            <option>+13:00</option>
                            <option>+14:00</option>
                        </select>
                        <div id="reg-time-zone-err" class="reg-err"></div>
                    </div>
                    <div>
                        <fmt:message var="regSubmit" bundle="${bundle}" key="web.reg.submit"/>
                        <input id="reg-submit" type="button" value="${regSubmit}">
                    </div>
                    <%--<div>--%>
                        <%--<input id="pic-in" type="file" name="file">--%>
                        <%--<input type="button" id="pic">--%>
                    <%--</div>--%>
                </div>
            </div>
        </div>
    </jsp:attribute>
</t:base>