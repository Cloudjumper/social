<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="lang" scope="session" value="${not empty sessionScope.lang ? sessionScope.lang : 'en'}"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="localization" var="bundle"/>
<jsp:useBean id="user" scope="request" type="org.drekorik.social.models.User"/>
<t:base>
    <jsp:attribute name="extraCss">
        <link rel="stylesheet" type="text/css" href="/static/css/user.css">
    </jsp:attribute>
    <jsp:attribute name="extraJs">
        <script rel="script" type="application/javascript" src="/static/js/user.js"></script>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <c:if test="${owner == true}">
            <script rel="script" type="application/javascript" src="/static/js/userOwner.js"></script>
        </c:if>
    </jsp:attribute>
    <jsp:attribute name="title">
        ${user.login}
    </jsp:attribute>
    <jsp:attribute name="content">
        <div class="user-info block">
            <div class="avatar-wrapper">
                <img class="avatar" src="${user.avatar.webPath}">
                <c:if test="${owner == true}">
                    <div id="avatar-change" class="avatar-change-wrapper">Change</div>
                    <input id="h-avatar-change" type="file" style="display: none"/>
                </c:if>
            </div>
            <div class="user-info-name">
                <div>${user.login}</div>
                <div><fmt:message bundle="${bundle}" key="web.user.firstName"/>: ${user.firstName}</div>
                <div><fmt:message bundle="${bundle}" key="web.user.secondName"/>: ${user.secondName}</div>
                <div><fmt:message bundle="${bundle}" key="web.user.birthDate"/>: ${user.birthDate}</div>
            </div>
        </div>
        <c:if test="${owner == true}">
            <div class="subscriptions block">
                <div class="subscriptions-title">
                    <fmt:message bundle="${bundle}" key="web.user.subscriptions"/>
                </div>
                <div class="subs">
                    <jsp:useBean id="subs" scope="request" type="java.util.List<org.drekorik.social.models.User>"/>
                    <c:forEach items="${subs}" var="sub">
                        <div class="sub">
                            <a href="/user/${sub.login}">
                                <img src="${sub.avatar.webPath}">
                            </a>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <div class="new-post block">
                <div class="new-post-text-area">
                    <fmt:message bundle="${bundle}" key="web.user.newPostPlaceholder" var="placeholder"/>
                    <textarea id="new-post-text" name="area" maxlength="300" placeholder="${placeholder}" style="resize: none"></textarea>
                </div>
                <div id="map">

                </div>
                <div class="new-post-buttons">
                    <div id="add-photo" class="new-post-buttons-add-photo">
                        <img src="/static/img/add_a_photo_black_54x54.png">
                    </div>
                    <input id="h-add-photo" type="file" style="display: none">
                    <div style="flex-grow: 1"></div>
                    <div class="new-post-buttons-submit">
                        <fmt:message bundle="${bundle}" key="web.user.submitPost" var="submit"/>
                        <input id="submit-post" type="button" value="${submit}">
                    </div>
                </div>
            </div>
        </c:if>
        <div id="posts">

        </div>
        <div id="navigation">
            <input disabled="disabled" id="prev" type="button" value="Back">
            <input disabled="disabled" id="next" type="button" value="Next">
        </div>
    </jsp:attribute>
</t:base>