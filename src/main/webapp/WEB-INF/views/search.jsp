<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="lang" scope="session" value="${not empty sessionScope.lang ? sessionScope.lang : 'en'}"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="localization" var="bundle"/>
<t:base>
    <jsp:attribute name="extraCss">
        <link rel="stylesheet" type="text/css" href="/static/css/search.css">
    </jsp:attribute>
    <jsp:attribute name="extraJs">
        <script rel="script" type="application/javascript" src="/static/js/search.js"></script>
    </jsp:attribute>
    <jsp:attribute name="title">
        News
    </jsp:attribute>
    <jsp:attribute name="content">
        <div class="block" style="display: flex">
            <div class="search-bar">
                <input type="text" id="search">
            </div>
            <div class="search-btn">
                <fmt:message bundle="${bundle}" key="web.search.find" var="find"/>
                <input id="find" type="button" value="${find}">
            </div>
        </div>
        <div class="search-results">
            <jsp:useBean id="users" scope="request" type="java.util.List<org.drekorik.social.models.mod.SearchUser>"/>
            <c:if test="${not empty users}">
                <c:forEach items="${users}" var="item">
                    <div class="user block">
                        <div class="user-data">
                            <div class="avatar-wrapper">
                                <img src="${item.avatar.webPath}">
                            </div>
                            <div class="data-wrapper">
                                <div class="data-first-name">${item.firstName}</div>
                                <div class="data-login">${item.login}</div>
                                <div class="data-second-name">${item.secondName}</div>
                            </div>
                        </div>
                        <div class="user-follow">
                            <c:choose>
                                <c:when test="${item.follow == false}">
                                    <div>
                                        <fmt:message bundle="${bundle}" key="web.search.follow" var="follow"/>
                                        <input class="follow" type="button" value="${follow}">
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div>
                                        <fmt:message bundle="${bundle}" key="web.search.stopFollow" var="stopFollow"/>
                                        <input class="stop-follow" type="button" value="${stopFollow}">
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>
    </jsp:attribute>
</t:base>