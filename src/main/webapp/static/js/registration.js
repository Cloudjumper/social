const formItems = {
    'login':$("#reg-login"),
    'pass':$("#reg-pass"),
    'email':$("#reg-email"),
    'firstName':$("#reg-first-name"),
    'secondName':$("#reg-second-name"),
    'birthDate':$("#reg-birth-date"),
    'zone':$("#reg-time-zone")
};

const formErrors = {
    'login':$("#reg-login-err"),
    'pass':$("#reg-pass-err"),
    'email':$("#reg-email-err"),
    'firstName':$("#reg-first-name-err"),
    'secondName':$("#reg-second-name-err"),
    'birthDate':$("#reg-birth-date-err"),
    'zone':$("#reg-time-zone-err")
};

$(document).ready(function () {
    // var today = new Date();
    const input = $('#reg-birth-date');
    // const year = today.getFullYear() - 18;
    // const month = today.getMonth();
    // const day = today.getDay();
    $.datetimepicker.setLocale('ru');
    input.datetimepicker({
        format:'d-m-Y',
        // I donno why this is not work, getDay() returns 0
        // value: (day>9?day:'0' + day) + '-' + (month>9?month:'0' + month) + '-' + (year),
        lang:'ru',
        maxDate: new Date(),
        timepicker: false
    });
});

$("#reg-submit").click(function () {
    $.each(formErrors, function (key, value) {
        value.text("");
        hideItem(value);
    });
    $.ajax({
        url: "/api/register",
        type: 'POST',
        contentType:'application/json',
        data: JSON.stringify({
            'login': $("#reg-login").val(),
            'pass': $("#reg-pass").val(),
            'email': $("#reg-email").val(),
            'firstName': $("#reg-first-name").val(),
            'secondName': $("#reg-second-name").val(),
            'birthDate': $("#reg-birth-date").val(),
            'zone': $("#reg-time-zone").val() + ":00"
        }),
        dataType:'json',
        success: function(response){
            window.location =  '/user';
        },
        error: function(response, status, error){
            $.each($.parseJSON(response.responseText), function (key, value) {
                formErrors[key].text(value);
                showItem(formErrors[key])
            })
        }
    });
});