function hideItem(item) {
    item.hide("fade", 200);
}

function showItem(item) {
    item.show("fade", 200);
}

String.format = function() {
    // The string containing the format items (e.g. "{0}")
    // will and always has to be the first argument.
    var theString = arguments[0];

    // start with the second argument (i = 1)
    for (var i = 1; i < arguments.length; i++) {
        // "gm" = RegEx options for Global search (more than one instance)
        // and for Multiline search
        var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
        theString = theString.replace(regEx, arguments[i]);
    }

    return theString;
};

function getCookie(cName) {
    var name = cName + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// function setCookie(cName, cValue) {
//     document.cookie =
// }

$("#header-log-action").click(function () {
    $("#log-in-form").toggle("fade", 200);
    $("#log-out-form").toggle("fade", 200);
});

$("#log-in").click(function () {
    const err = $("#log-in-form-err");
    hideItem(err);
    $.ajax({
        url: "/api/auth",
        type: "POST",
        contentType: 'application/json',
        data: JSON.stringify({
            'login': $("#login").val(),
            'pass': $("#pass").val()
        }),
        dataType: 'json',
        success: function (response) {
            window.location = '/user';
        },
        error: function (response, status, error) {
            console.log(response.responseText);
            $.each($.parseJSON(response.responseText), function (key, value) {
                err.text("");
                err.text(value);
                showItem(err);
            })
        }
    })
});

$("#log-out").click(function () {
    $.ajax({
        url: "/api/auth",
        type: "DELETE",
        contentType: 'application/json; charset=UTF-8',
        success: function (response) {
            window.location.replace('/registration');
        }
    });
});

$("#footer-lang-select").change(function () {
    $.ajax({
        url: "/api/lang",
        type: "POST",
        data: {
            lang: $("#footer-lang-select").find("option:selected").val()
        },
        success: function (response) {
            location.reload();
        },
        error: function (response, status, error) {

        }
    });
});