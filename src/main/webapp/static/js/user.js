var postTemplate =
    "<div class=\"post block\">" +
    "   <div class=\"post-header\">" +
    "       <div class=\"post-header-avatar\">" +
    "           <img src=\"{0}\">" +
    "       </div>" +
    "       <div class=\"post-header-author\">" +
    "           {1}" +
    "           {2}" +
    "           {3}" +
    "       </div>" +
    "   </div>" +
    "   <div class=\"post-text\">" +
    "       {4}" +
    "   </div>" +
    "   <div class='post-map' id=\"{5}\">" +
    "       " +
    "   </div>"+
    "{6}" +
    "{7}" +
    "</div>";

var postPictureTemplate =
    "<div class=\"post-img\">" +
    "   <img src=\"{0}\">" +
    "</div>";

var postDeleteTemplate =
    "<div>" +
    "   <input class=\"h-post-id\" value=\"{0}\" hidden=\"hidden\">" +
    "   <input class=\"post-delete\" value=\"Delete\" type=\"button\">" +
    "</div>";

var page = 1;
var user;

$(document).on("click", ".post-delete" ,function () {
    console.log("del");
    $.ajax({
        url: "/api/post?q=" + $(this).parent().find(".h-post-id").val(),
        type: 'DELETE',
        success: function (response) {
            location.reload();
        },
        error: function (response, status, error) {
            console.log(response.responseText)
        }
    })
});

$(document).ready(function () {
    ymaps.ready(function () {
        var arr = location.pathname.split("/");
        user = arr[arr.length - 1];
        $.ajax({
            url: "/api/post?p=" + page + "&u=" + user,
            type: "get",
            // contentType: 'application/json',
            // data: {p: 1},
            dataType: 'json',
            success: function (response) {
                var res = response;
                var picture;
                var del;
                var map;
                var mapId;
                $.each(res['posts'], function (index, value) {
                    if (value['postPictureId'] != 'null')
                        picture = String.format(postPictureTemplate, value['postPictureWebPath']);
                    else
                        picture = "";
                    if (res['owner'] == true)
                        del = String.format(postDeleteTemplate, value['postId']);
                    else
                        del ="";
                    mapId = "map" + value['postId'];
                    $("#posts").append(String.format(postTemplate,
                        value['postUserAvatarWebPath'],
                        value['postUserFirstName'],
                        value['postUserLogin'],
                        value['postUserSecondName'],
                        value['postText'],
                        mapId,
                        picture, del));
                    map = new ymaps.Map(mapId, {
                        center: [value['postLat'], value['postLon']],
                        zoom: 15,
                        controls: []
                    });
                    point = new ymaps.Placemark([value['postLat'], value['postLon']], {}, {
                        preset: 'islands#glyphCircleIcon'
                    });
                    map.geoObjects.add(point);
                });
                if (res['nextPage'] == true)
                    $("#next").prop('disabled', false);
                else
                    $("#next").prop('disabled', true);
                if (res['prevPage'] == true)
                    $("#prev").prop('disabled', false);
                else
                    $("#prev").prop('disabled', true);
            },
            error: function (response, status, error) {
                console.log(response.responseText);
            }
        })
    });
});

$("#prev").click(function () {
    $("#posts").empty();
    page -= 1;
    $.ajax({
        url: "/api/post?p=" + page + "&u=" + user,
        type: "get",
        dataType: 'json',
        success: function (response) {
            var res = response;
            var picture;
            var del;
            var map;
            var mapId;
            $.each(res['posts'], function (index, value) {
                if (value['postPictureId'] != 'null')
                    picture = String.format(postPictureTemplate, value['postPictureWebPath']);
                else
                    picture = "";
                if (res['owner'] == true)
                    del = String.format(postDeleteTemplate, value['postId']);
                else
                    del ="";
                mapId = "map" + value['postId'];
                $("#posts").append(String.format(postTemplate,
                    value['postUserAvatarWebPath'],
                    value['postUserFirstName'],
                    value['postUserLogin'],
                    value['postUserSecondName'],
                    value['postText'],
                    mapId,
                    picture, del));
                map = new ymaps.Map(mapId, {
                    center: [value['postLat'], value['postLon']],
                    zoom: 15,
                    controls: []
                });
                point = new ymaps.Placemark([value['postLat'], value['postLon']], {}, {
                    preset: 'islands#glyphCircleIcon'
                });
                map.geoObjects.add(point);
            });
            if (res['nextPage'] == true)
                $("#next").prop('disabled', false);
            else
                $("#next").prop('disabled', true);
            if (res['prevPage'] == true)
                $("#prev").prop('disabled', false);
            else
                $("#prev").prop('disabled', true);
        },
        error: function (response, status, error) {
            console.log(response.responseText);
        }
    })
});

$("#next").click(function () {
    $("#posts").empty();
    page += 1;
    $.ajax({
        url: "/api/post?p=" + page + "&u=" + user,
        type: "get",
        dataType: 'json',
        success: function (response) {
            var res = response;
            var picture;
            var del;
            var map;
            var mapId;
            $.each(res['posts'], function (index, value) {
                if (value['postPictureId'] != 'null')
                    picture = String.format(postPictureTemplate, value['postPictureWebPath']);
                else
                    picture = "";
                if (res['owner'] == true)
                    del = String.format(postDeleteTemplate, value['postId']);
                else
                    del = "";
                mapId = "map" + value['postId'];
                $("#posts").append(String.format(postTemplate,
                    value['postUserAvatarWebPath'],
                    value['postUserFirstName'],
                    value['postUserLogin'],
                    value['postUserSecondName'],
                    value['postText'],
                    mapId,
                    picture, del));
                map = new ymaps.Map(mapId, {
                    center: [value['postLat'], value['postLon']],
                    zoom: 15,
                    controls: []
                });
                point = new ymaps.Placemark([value['postLat'], value['postLon']], {}, {
                    preset: 'islands#glyphCircleIcon'
                });
                map.geoObjects.add(point);
            });
            if (res['nextPage'] == true)
                $("#next").prop('disabled', false);
            else
                $("#next").prop('disabled', true);
            if (res['prevPage'] == true)
                $("#prev").prop('disabled', false);
            else
                $("#prev").prop('disabled', true);
        },
        error: function (response, status, error) {
            console.log(response.responseText);
        }
    })
});