var postTemplate =
    "<div class=\"post block\">" +
    "   <div class=\"post-header\">" +
    "       <div class=\"post-header-avatar\">" +
    "           <img src=\"{0}\">" +
    "       </div>" +
    "       <div class=\"post-header-author\">" +
    "           {1}" +
    "           {2}" +
    "           {3}" +
    "       </div>" +
    "   </div>" +
    "   <div class=\"post-text\">" +
    "       {4}" +
    "   </div>" +
    "   <div class='post-map' id=\"{5}\">" +
    "       " +
    "   </div>"+
    "{6}" +
    "</div>";

var postPictureTemplate =
    "   <div class=\"post-image\">" +
    "       <img src=\"{0}\">" +
    "   </div>";

var page = 1;

$(document).ready(function () {
    ymaps.ready(function () {
        $.ajax({
            url: "/api/news?p=" + page,
            type: "get",
            // contentType: 'application/json',
            // data: {p: 1},
            dataType: 'json',
            success: function (response) {
                console.log(response);
                var res = response;
                var picture;
                var map;
                var mapId;
                $.each(res['posts'], function (index, value) {
                    if (value['postPictureId'] != 'null')
                        picture = String.format(postPictureTemplate, value['postPictureWebPath']);
                    else
                        picture = "";
                    mapId = "map" + value['postId'];
                    $("#posts").append(String.format(postTemplate,
                        value['postUserAvatarWebPath'],
                        value['postUserFirstName'],
                        value['postUserLogin'],
                        value['postUserSecondName'],
                        value['postText'],
                        mapId,picture));
                    map = new ymaps.Map(mapId, {
                        center: [value['postLat'], value['postLon']],
                        zoom: 15,
                        controls: []
                    });
                    point = new ymaps.Placemark([value['postLat'], value['postLon']], {}, {
                        preset: 'islands#glyphCircleIcon'
                    });
                    map.geoObjects.add(point);
                });
                if (res['nextPage'] == true)
                    $("#next").prop('disabled', false);
                else
                    $("#next").prop('disabled', true);
                if (res['prevPage'] == true)
                    $("#prev").prop('disabled', false);
                else
                    $("#prev").prop('disabled', true);
            },
            error: function (response, status, error) {
                console.log(response.responseText);
            }
        })
    });
});

$("#prev").click(function () {
    $("#posts").empty();
    page -= 1;
    $.ajax({
        url: "/api/news?p=" + page,
        type: "get",
        dataType: 'json',
        success: function (response) {
            var res = response;
            var picture;
            var map;
            var mapId;
            $.each(res['posts'], function (index, value) {
                if (value['postPictureId'] != 'null')
                    picture = String.format(postPictureTemplate, value['postPictureWebPath']);
                else
                    picture = "";
                mapId = "map" + value['postId'];
                $("#posts").append(String.format(postTemplate,
                    value['postUserAvatarWebPath'],
                    value['postUserFirstName'],
                    value['postUserLogin'],
                    value['postUserSecondName'],
                    value['postText'],
                    mapId,picture));
                map = new ymaps.Map(mapId, {
                    center: [value['postLat'], value['postLon']],
                    zoom: 15,
                    controls: []
                });
                point = new ymaps.Placemark([value['postLat'], value['postLon']], {}, {
                    preset: 'islands#glyphCircleIcon'
                });
                map.geoObjects.add(point);
            });
            if (res['nextPage'] == true)
                $("#next").prop('disabled', false);
            else
                $("#next").prop('disabled', true);
            if (res['prevPage'] == true)
                $("#prev").prop('disabled', false);
            else
                $("#prev").prop('disabled', true);
        },
        error: function (response, status, error) {
            console.log(response.responseText);
        }
    })
});

$("#next").click(function () {
    $("#posts").empty();
    page += 1;
    $.ajax({
        url: "/api/news?p=" + page,
        type: "get",
        dataType: 'json',
        success: function (response) {
            var res = response;
            var picture;
            var map;
            var mapId;
            $.each(res['posts'], function (index, value) {
                if (value['postPictureId'] != 'null')
                    picture = String.format(postPictureTemplate, value['postPictureWebPath']);
                else
                    picture = "";
                mapId = "map" + value['postId'];
                $("#posts").append(String.format(postTemplate,
                    value['postUserAvatarWebPath'],
                    value['postUserFirstName'],
                    value['postUserLogin'],
                    value['postUserSecondName'],
                    value['postText'],
                    mapId,picture));
                map = new ymaps.Map(mapId, {
                    center: [value['postLat'], value['postLon']],
                    zoom: 15,
                    controls: []
                });
                point = new ymaps.Placemark([value['postLat'], value['postLon']], {}, {
                    preset: 'islands#glyphCircleIcon'
                });
                map.geoObjects.add(point);
            });
            if (res['nextPage'] == true)
                $("#next").prop('disabled', false);
            else
                $("#next").prop('disabled', true);
            if (res['prevPage'] == true)
                $("#prev").prop('disabled', false);
            else
                $("#prev").prop('disabled', true);
        },
        error: function (response, status, error) {
            console.log(response.responseText);
        }
    })
});