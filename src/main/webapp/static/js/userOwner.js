var point;
var lat = 0;
var lon = 0;
var map;

$(document).ready(function () {
    ymaps.ready(function () {
        ymaps.geolocation.get({
            provider: 'yandex'
        }).then(function (result) {
            lat = result.geoObjects.position[0];
            lon = result.geoObjects.position[1];

            map = new ymaps.Map('map', {
                center: [lat, lon],
                zoom: 15,
                controls: []
            });

            map.events.add('click', function (e) {
                map.geoObjects.remove(point);
                var coords = e.get('coords');
                lat = coords[0].toPrecision(6);
                lon = coords[1].toPrecision(6);
                point = new ymaps.Placemark([lat, lon], {}, {
                    preset: 'islands#glyphCircleIcon'
                });
                map.geoObjects.add(point);
            });
        });
    });
});

$("#h-avatar-change").change(function () {
    var file_data = $("#h-avatar-change").prop("files")[0];   // Getting the properties of file from file field
    $("#h-avatar-change").val(null);
    var form_data = new FormData();                  // Creating object of FormData class
    form_data.append("pic", file_data);             // Appending parameter named file with properties of file_field to form_data
    form_data.append("description", "desc");         // Adding extra parameters to form_data
    $.ajax({
        url: "/api/avatar",
        // dataType: 'multipart/form-data',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         // Setting the data attribute of ajax with file_data
        type: 'post',
        success: function (response) {
            location.reload();
        },
        error: function (response, status, error) {
            console.log(response.responseText)
        }
    })
});

$("#avatar-change").click(function () {
    $("#h-avatar-change").click();
});

$("#add-photo").click(function () {
    $("#h-add-photo").click();
});

$("#submit-post").click(function () {
    var form_data = new FormData();
    if ($("#h-add-photo").prop("files")[0] != null ){
        var file_data = $("#h-add-photo").prop("files")[0];
        form_data.append("pic", file_data);
    }
    $("#h-add-photo").val(null);
    form_data.append("text", $("#new-post-text").val());
    form_data.append("lat", lat);
    form_data.append("lon", lon);
    $.ajax({
        url: "/api/post",
        // dataType: 'multipart/form-data',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         // Setting the data attribute of ajax with file_data
        type: 'post',
        success: function (response) {
            location.reload();
        },
        error: function (response, status, error) {
            console.log(response.responseText)
        }
    })
});