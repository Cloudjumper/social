$("#find").click(function () {
   location.replace("/search?q=" + $("#search").val())
});

$(".follow").click(function () {
    console.log($(this).parent().parent().text());
    $.ajax({
        url: "/api/follow?u=" + $.trim($(this).parent().parent().parent().find(".data-login").text()),
        type: "POST",
        success: function (response) {
            location.reload();
        },
        error: function (response, status, error) {

        }
    })
});

$(".stop-follow").click(function () {
    console.log($(this).parent().parent().text());
    $.ajax({
        url: "/api/follow?u=" + $.trim($(this).parent().parent().parent().find(".data-login").text()),
        type: "DELETE",
        success: function (response) {
           location.reload();
        },
        error: function (response, status, error) {

        }
    })
});